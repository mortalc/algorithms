﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Common
{
    public class ConvertToBase
    {
        public static int[] GetBaseRepresentation(int number, int toBaseOfNumber = 2, bool withoutLeadingZeros = true)
        {
            int[] arr = new int[32];
            int reminder = 0,
                chastnoe = number,
                i = 32;
            do
            {
                chastnoe = number / toBaseOfNumber;
                reminder = number % toBaseOfNumber;
                if (reminder < 0)
                {
                    reminder = -reminder;
                    chastnoe += reminder;
                }
                number = chastnoe;
                arr[--i] = reminder;
            }
            while (chastnoe != 0 && i != 0);

            if (withoutLeadingZeros)
            {
                bool add = false;
                List<int> retArr = new List<int>();
                foreach (int val in arr)
                {
                    if (val > 0 && !add)
                        add = true;
                    if (add)
                        retArr.Add(val);
                }
                return retArr.ToArray<int>();
            }

            return arr;
        }
    }
}
