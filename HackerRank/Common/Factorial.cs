﻿using System.Collections.Generic;

namespace Utilities.Common
{
    public class Factorial
    {
        static Dictionary<int, int> factorial = new Dictionary<int, int>();

        public static int CountFactorial(int N)
        {
            if (N == 0 || N == 1) return 1;
            if (N == 2) return 2;
            int product = 2;
            if (factorial.ContainsKey(N))
            {
                factorial.TryGetValue(N, out product);
                return product;
            }
            for (int i = 3; i <= N; i++)
            {
                if (!factorial.ContainsKey(i))
                {
                    product = product * i;
                    factorial.Add(i, product);
                }
                else
                {
                    factorial.TryGetValue(i, out product);
                }
            }

            return product;
        }
    }
}
