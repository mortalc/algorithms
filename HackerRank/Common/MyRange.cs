﻿using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Common
{
    public class MyRange
    {
        public MyRange(int r, int s, int e)
        {
            row = r;
            start = s;
            end = e;
        }
        public int row { get; set; }
        public int start { get; set; }
        public int end { get; set; }
    }

    public class MyRangeRepo
    {
        public List<MyRange> list = new List<MyRange>();

        public void AddRange(int[] arr)
        {
            MyRange range;
            range = list.Where(x => x.row == arr[0]).ToList<MyRange>().Where(x => x.start >= arr[1] && x.end <= arr[2]).FirstOrDefault();
            if (range != null)
            {
                // out MyRange
                range.start = arr[1];
                range.end = arr[2];
            }

            else
            {
                range = list.Where(x => x.row == arr[0]).ToList<MyRange>().Where(x => x.end >= arr[1] && x.end <= arr[2]).FirstOrDefault();
                if (range != null)
                {
                    // right intersection
                    range.end = arr[2];
                }
                else
                {
                    range = list.Where(x => x.row == arr[0]).ToList<MyRange>().Where(x => x.start >= arr[1] && x.start <= arr[2]).FirstOrDefault();
                    if (range != null)
                    {
                        // left intersection
                        range.start = arr[1];
                    }
                    else
                    {
                        range = list.Where(x => x.row == arr[0]).ToList<MyRange>().Where(x => x.start <= arr[1] && x.end >= arr[2]).FirstOrDefault();
                        if (range != null)
                        {// inner MyRange
                        }
                        else
                        {
                            MyRange r = new MyRange(arr[0], arr[1], arr[2]);
                            list.Add(r);
                        }
                    }
                }
            }
            //JoinRanges(); need to call explicitly after adding ranges

        }

        public void JoinRanges()
        {
            for (int i = 0; i < list.Count; i++)
            {
                //MyRange range;
                foreach (var range in list.Where(x => x.row == list[i].row).ToList<MyRange>().Where(x => x.start >= list[i].start && x.start <= list[i].end && x.end >= list[i].end))
                {

                    if (list.IndexOf(range) != i)
                    {
                        list[i].end = range.end;

                        list.Remove(range);
                    }
                    //    Console.WriteLine("row " + range.row + ", start " + range.start + ", end " + range.end + " len " + list.Count);    

                }
                //else
                //{
                foreach (var range in list.Where(x => x.row == list[i].row).ToList<MyRange>().Where(x => x.start <= list[i].start && x.end >= list[i].start && x.end <= list[i].end))
                {

                    if (list.IndexOf(range) != i)
                    {
                        list[i].start = range.start;
                        list.Remove(range);
                        //Console.WriteLine("row " + range.row + ", start " + range.start + ", end " + range.end + " len " + list.Count);
                    }

                }
                //} 
                /*
                foreach(var range in list.Where(x=> x.row == list[i].row).ToList<MyRange>().Where(x=> x.start >= list[i].start && x.end <= list[i].end  ))
                    {
                    
                        if(list.IndexOf(range) != i)
                        {    
                        list.Remove(range);
                        //Console.WriteLine("row " + range.row + ", start " + range.start + ", end " + range.end + " len " + list.Count);
                        }    
                    
                   }
                   */
            }
        }

    }
}
