﻿using System;
using System.IO;
using System.Numerics;

namespace Utilities.Common
{
    public static class Common
    {
        public static int ReadInt()
        {
            //var read = Console.ReadLine();
            return int.Parse(Console.ReadLine());
        }

        public static int[] ReadIntArr()
        {
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);
            return arr;
        }

        public static T[] ReadArr<T>(Converter<string, T> converter) where T : struct
        {
            return ReadArr<T>(Console.ReadLine(), converter);
            /*
            string[] arr_temp = Console.ReadLine().Split(' ');
            T[] arr = Array.ConvertAll(arr_temp, converter);
            return arr;
             */ 
        }

        public static T[] ReadArr<T>(string str, Converter<string, T> converter) where T : struct
        {
            string[] arr_temp = str.Split(' ');
            T[] arr = Array.ConvertAll(arr_temp, converter);
            return arr;
        }

        public static int[][] ReadIntIntArr(int numberOfRecords)
        {
            int[][] a = new int[numberOfRecords][];
            for (int a_i = 0; a_i < numberOfRecords; a_i++)
            {
                a[a_i] = ReadIntArr();
            }
            return a;
        }

        public static int[] SplitToIntArray(this string str)
        {
            int[] arr = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                arr[i] = int.Parse(str[i].ToString());
            }

            return arr;
        }

        public static string[] ReadLinesFromFile(string filename)
        {
            /*
            List<string> arr = new List<string>();
            using (StreamReader fs = new StreamReader(filename))
            {
                while (true)
                {
                    string tempStr = fs.ReadLine();

                    if (tempStr == null) break;

                    arr.Add(tempStr);
                }
            }
            return arr.ToArray();
            */
            return File.ReadAllLines(filename);
        }

        public static void WriteStringArrayToFile(string[] arr, string filename)
        {
            File.WriteAllLines(filename, arr);
        }

        public static BigInteger PowerBI(long x, long y, long modulo = 1000000007)
        {
            //long modulo = 1000000000 + 7;
            if (y == 0)
                return 1;

            BigInteger temp = PowerBI(x, y / 2);
            //temp = BigInteger.Remainder(temp, new BigInteger(modulo));
            if (y % 2 == 0)
                return BigInteger.Remainder(BigInteger.Multiply(temp, temp), new BigInteger(modulo));
            else
                return BigInteger.Remainder(BigInteger.Multiply(BigInteger.Multiply(new BigInteger(x), temp), temp), new BigInteger(modulo));
        }

        static long Power(long x, long y)
        {
            //long modulo = 1000000000 + 7;
            long temp;
            if (y == 0)
                return 1;
            temp = Power(x, y / 2);
            if (y % 2 == 0)
                return temp * temp;// % modulo;
            else
                return x * temp * temp;// % modulo;
        }

        /* Iterative Function to calculate (x^y) in O(logy) */
        static long PowerIT(long x, long y)
        {
            long res = 1;     // Initialize result

            while (y > 0)
            {
                // If y is odd, multiply x with result
                if (y % 2 == 1)
                    res = res * x;

                // n must be even now
                y /= 2; // y = y/2
                x = x * x;  // Change x to x^2
            }
            return res;
        }

        /* Iterative Function to calculate (x^n)%p in O(logy) */
        static long PowerIT(long x, long y, long modulo = 1000000007)
        {
            long res = 1;      // Initialize result

            x = x % modulo;  // Update x if it is more than or 
            // equal to p

            while (y > 0)
            {
                // If y is odd, multiply x with result
                if (y % 2 == 1)
                    res = (res * x) % modulo;

                // y must be even now
                y /= 2; // y = y/2
                x = (x * x) % modulo;
            }
            return res;
        }
    }


}
