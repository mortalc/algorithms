﻿using System;
using System.Collections.Generic;
//using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Search
{
    public class BST<Key, Value> where Key : IComparable<Key>
    {
        private Node root;

        public virtual void Put(Key key, Value val)
        {
            root = Put(root, key, val);
        }

        public bool IsEmpty()
        {
            return root == null;
        }

        private Node Put(Node x, Key key, Value val)
        {
            if (x == null) return new Node(key, val);

            int cmp = key.CompareTo(x.Key);
            if (cmp < 0) x.Left = Put(x.Left, key, val);
            else if (cmp > 0) x.Right = Put(x.Right, key, val);
            else x.Val = val;

            x.Count = Size(x.Left) + Size(x.Right) + 1;
            return x;
        }


        public Value Get(Key key)
        {
            Node x = root;
            while (x != null)
            {
                int cmp = key.CompareTo(x.Key);

                if (cmp < 0) x = x.Left;
                else if (cmp > 0) x = x.Right;
                else return x.Val;
            }
            return default(Value);
        }


        public bool Contains(Key key)
        {
            Value defVal = default(Value);
            Value act = Get(key);
            return act != null && !(act.Equals(defVal));
        }

        //public IEnumerator<Key> GetEnumerator()
        //{
        //    //throw new NotImplementedException();
        //    //yield return default(ElementType);
        //}

        public IEnumerable<Key> Keys()
        {
            Queue<Key> q = new Queue<Key>();
            inorder(root, q);
            return q;
        }

        private void inorder(Node x, Queue<Key> q)
        {
            if (x == null) return;
            inorder(x.Left, q);
            q.Enqueue(x.Key);
            inorder(x.Right, q);
        }

        public virtual Key Min()
        {
            return Min(root).Key;
        }

        private Node Min(Node x)
        {
            if (x.Left == null) return x;
            return Min(x.Left);
        }

        public Key Max()
        {
            return Max(root).Key;
        }

        private Node Max(Node x)
        {
            if (x.Right == null) return x;
            return Max(x.Right);
        }

        public int Rank(Key key)
        {
            return Rank(root, key);
        }

        private int Rank(Node x, Key key) // How many keys < k 
        {
            if (x == null) return 0;

            int cmp = key.CompareTo(x.Key);

            if (cmp < 0) return Rank(x.Left, key);
            else if (cmp > 0) return 1 + Size(x.Left) + Rank(x.Right, key);
            else return Size(x.Left);
        }

        public Key Select(int k)
        {
            return Select(root, k).Key;
        }

        private Node Select(Node x, int k)
        {
            // Возвращает узел, содержащий ключ ранга k
            if (x == null) return null;

            int t = Size(x.Left);

            if (t > k) return Select(x.Left, k);
            else if (t < k) return Select(x.Right, k - t - 1);
            else return x;
        }

        public Key Floor(Key key) // Largest key ≤ a given key.
        {
            Node x = Floor(root, key);
            if (x == null) return default(Key);

            return x.Key;
        }

        private Node Floor(Node x, Key key) // Largest key ≤ a given key.
        {
            int cmp = key.CompareTo(x.Key);

            if (x == null) return null;

            // Case 1. [k equals the key at root]
            //The floor of k is k.            
            if (cmp == 0) return x;

            //            Case 2. [k is less than the key at root]
            //            The floor of k is in the left subtree.
            if (cmp < 0) return Floor(x.Left, key);

            //          Case 3. [k is greater than the key at root]
            //          The floor of k is in the right subtree
            //          (if there is any key ≤ k in right subtree);
            //          otherwise it is the key in the root.
            Node t = Floor(x.Right, key);
            if (t != null) return t;
            else return x;
        }

        public Key Ceiling(Key key) //Smallest key ≥ a given key.
        {
            Node x = Floor(root, key);
            if (x == null) return default(Key);

            return x.Key;
        }

        private Node Ceiling(Node x, Key key) //Smallest key ≥ a given key.
        {
            int cmp = key.CompareTo(x.Key);

            if (x == null) return null;

            // Case 1. [k equals the key at root]
            //The floor of k is k.            
            if (cmp == 0) return x;

            if (cmp > 0) return Ceiling(x.Left, key);

            Node t = Ceiling(x.Left, key);
            if (t != null) return t;
            else return x;
        }

        public virtual int Size()
        {
            return Size(root);
        }

        private int Size(Node x)
        {
            if (x == null) return 0;
            return x.Count;
        }

        public virtual void DeleteMin()
        {
            root = DeleteMin(root);
        }

        private Node DeleteMin(Node x)
        {
            if (x.Left == null) return x.Right;
            x.Left = DeleteMin(x.Left);
            x.Count = Size(x.Left) + Size(x.Right) + 1;
            return x;
        }

        public virtual void DeleteMax()
        {
            root = DeleteMax(root);
        }

        private Node DeleteMax(Node x)
        {
            if (x.Right == null) return x.Left;
            x.Right = DeleteMax(x.Right);
            x.Count = Size(x.Left) + Size(x.Right) + 1;
            return x;
        }

        public virtual void Delete(Key key)
        {
            root = Delete(root, key);
        }

        private Node Delete(Node x, Key key)
        {
            if (x == null) return null;
            int cmp = key.CompareTo(x.Key);
            if (cmp < 0) x.Left = Delete(x.Left, key);
            else if (cmp > 0) x.Right = Delete(x.Right, key);
            else
            {
                if (x.Right == null) return x.Left;
                if (x.Left == null) return x.Right;
                Node t = x;
                x = Min(t.Right);
                x.Right = DeleteMin(t.Right);
                x.Left = t.Left;
            }
            x.Count = Size(x.Left) + Size(x.Right) + 1;
            return x;
        }

        class Node
        {
            public Key Key { get; set; }
            public Value Val { get; set; }
            public Node Left, Right;
            public int Count { get; set; }

            public Node(Key k, Value v)
            {
                this.Key = k;
                this.Val = v;
            }
        }
    }
}
