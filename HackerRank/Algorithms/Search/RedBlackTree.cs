﻿using System;

namespace Utilities.Algorithms.Search
{
    public class RedBlackTree<Key, Value> : BST<Key, Value> where Key : IComparable<Key>
    {
        Node root;
        private const bool RED = true;
        private const bool BLACK = false;

        public override void Put(Key key, Value val)
        {
            root = Put(root, key, val);
            root.Color = BLACK;
        }

        private Node Put(Node h, Key key, Value val)
        {
            if (h == null)
                return new Node(key, val, 1, RED);

            int cmp = key.CompareTo(h.Key);
            if (cmp < 0) h.Left = Put(h.Left, key, val);
            else if (cmp > 0) h.Right = Put(h.Right, key, val);
            else h.Value = val;

            if (IsRed(h.Right) && !IsRed(h.Left)) h = RotateLeft(h);
            if (IsRed(h.Left) && IsRed(h.Left.Left)) h = RotateRight(h);
            if (IsRed(h.Left) && IsRed(h.Right)) FlipColors(h);

            h.Count = 1 + Size(h.Left) + Size(h.Right);

            return h;
        }

        public override int Size()
        {
            return Size(root);
        }

        private int Size(Node x)
        {
            if (x == null) return 0;
            return x.Count;
        }


        private bool IsRed(Node n)
        {
            if (n == null) return false; // null links are black
            return n.Color == RED;
        }

        private Node RotateLeft(Node h)
        {
            Node x = h.Right;
            h.Right = x.Left;
            x.Left = h;
            x.Color = h.Color;
            h.Color = RED;
            return x;
        }

        private Node RotateRight(Node h)
        {
            Node x = h.Left;
            h.Left = x.Right;
            x.Right = h;
            x.Color = h.Color;
            h.Color = RED;
            return x;
        }

        private void FlipColors(Node h)
        {
            h.Color = !h.Color;
            h.Left.Color = !h.Left.Color;
            h.Right.Color = !h.Right.Color;
        }

        private Node MoveRedLeft(Node h)
        {
            // если узел h красный, а обе ссылки h.left и h.left.left черные
            // то окрашиваем h.left или один из его потомков красным
            FlipColors(h);
            if (IsRed(h.Right.Left))
            {
                h.Right = RotateRight(h.Right);
                h = RotateLeft(h);
                FlipColors(h);
            }
            return h;
        }

        private Node MoveRedRight(Node h)
        {
            // если узел h красный, а обе ссылки h.left и h.right.left черные
            // то окрашиваем h.right или один из его потомков красным
            FlipColors(h);
            if (IsRed(h.Left.Left))
            {
                h = RotateRight(h);
                FlipColors(h);
            }
            return h;
        }

        public override void DeleteMin()
        {
            if (!IsRed(root.Left) && !IsRed(root.Right))
            {
                root.Color = RED; 
            }
            root = DeleteMin(root);
            if (!IsEmpty())
            {
                root.Color = BLACK;
            }
        }

        private Node DeleteMin(Node h)
        {
            if (h.Left == null)
                return null;
            if (!IsRed(h.Left) && !IsRed(h.Left.Left))
            {
                h = MoveRedLeft(h);
            }
            h.Left = DeleteMin(h.Left);
            return Balance(h);
        }

        public override void DeleteMax()
        {
            if (IsEmpty()) return;

            // if both children of root are black, set root to red
            if (!IsRed(root.Left) && !IsRed(root.Right))
                root.Color = RED;

            root = DeleteMax(root);
            if (!IsEmpty()) root.Color = BLACK;
        }

        private Node DeleteMax(Node h)
        {
            if (IsRed(h.Left))
                h = RotateRight(h);

            if (h.Right == null)
                return null;

            if (!IsRed(h.Right) && !IsRed(h.Right.Left))
                h = MoveRedRight(h);

            h.Right = DeleteMax(h.Right);

            return Balance(h);
        }

        private Node Balance(Node h)
        {
            if (IsRed(h.Right)) h = RotateLeft(h);

            if (IsRed(h.Left) && IsRed(h.Left.Left)) h = RotateRight(h);
            if (IsRed(h.Left) && IsRed(h.Right)) FlipColors(h);

            h.Count = 1 + Size(h.Left) + Size(h.Right);

            return h;
        }

        public override void Delete(Key key)
        {
            if (key == null) return;
            if (!Contains(key)) return;

            // if both children of root are black, set root to red
            if (!IsRed(root.Left) && !IsRed(root.Right))
                root.Color = RED;

            root = Delete(root, key);
            if (!IsEmpty()) root.Color = BLACK;
            // assert check();
        }

        private Node Delete(Node h, Key key)
        {
            if (key.CompareTo(h.Key) < 0)
            {
                if (!IsRed(h.Left) && !IsRed(h.Left.Left))
                    h = MoveRedLeft(h);
                h.Left = Delete(h.Left, key);
            }
            else
            {
                if (IsRed(h.Left)) 
                    h = RotateRight(h);
                if (key.CompareTo(h.Key) == 0 && (h.Right == null))
                    return null;
                if (!IsRed(h.Right) && !IsRed(h.Right.Left))
                    h = MoveRedRight(h);
                if (key.CompareTo(h.Key) == 0)
                {
                    Node x = Min(h.Right);
                    h.Key = x.Key;
                    h.Value = x.Value;
                    h.Right = DeleteMin(h.Right);
                }
                else h.Right = Delete(h.Right, key);
            }
            return Balance(h);
        }

        public override Key Min()
        {
            return Min(root).Key;
        }

        private Node Min(Node x)
        {
            if (x.Left == null) return x;
            return Min(x.Left);
        }

        private class Node
        {
            public Key Key { get; set; }
            public Value Value { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
            public bool Color { get; set; }
            public int Count;

            public Node(Key k, Value v, int count, bool color)
            {
                Key = k;
                Value = v;
                Color = color;
                Count = count;
            }
        }
    }
}
