﻿using System;

namespace Utilities.Algorithms.DynamicProgr
{
    public class MaxSubarray
    {
        // максимальная сумма последовательных элементов
        public static int CountContigousSubArraySum(int[] a, out int leftPos, out int rightPos)
        {
            int maxEndingHere = a[0], maxSoFar = a[0];
            leftPos = rightPos = 0;

            if (a.Length == 0)
                return 0;

            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] > maxEndingHere + a[i])
                    leftPos = i;
                maxEndingHere = Math.Max(a[i], maxEndingHere + a[i]);

                if (maxSoFar < maxEndingHere)
                {
                    rightPos = i;
                }
                maxSoFar = Math.Max(maxSoFar, maxEndingHere);
            }
            return maxSoFar;
        }
    }
}
