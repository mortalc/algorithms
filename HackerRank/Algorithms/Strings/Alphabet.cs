﻿namespace Utilities.Algorithms.Strings
{
    public class Alphabet
    {
        string _s;
        public Alphabet(string s)
        {
            _s = s;
        }

        public char ToChar(int index)
        {
            return _s[index];
        }

        public int ToIndex(char c)
        {
            // преобразование с в индекс от 0 до R-1
            return 0;
        }

        public bool Contains(char c)
        {
            return false;
        }

        public int R()
        { // основание, количество символов в алфавите
            return _s.Length;
        }

        public int LgR()
        {
            // количство битов для представления индекса
            return 0;
        }

        public int[] ToIndicies(string s)
        {
            // преобразование s  в целое число по основанию R
            return null;
        }

        public string ToChars(int[] indicies)
        {
            // преобразование R-ичного целого числа в строку этого алфавита
            return string.Empty;
        }

    }
}
