﻿using System;
using Utilities.Algorithms.Strings.Sorts;

namespace HackerRank.Algorithms.Strings
{
    public class LongestCommonSubsequence
    {
        public static int LCSLength(string s1, string s2)
        {
            int[][] c = new int[s1.Length + 1][];
            for (int i = 0; i <= s1.Length; i++)
                c[i] = new int[s2.Length + 1];

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                    if (s1.CharAt(i - 1) == s2.CharAt(j - 1))
                        c[i][j] = c[i - 1][j - 1] + 1;
                    else
                        c[i][j] = Math.Max(c[i - 1][j], c[i][j - 1]);

            return c[s1.Length][s2.Length];
        }
    }
}
