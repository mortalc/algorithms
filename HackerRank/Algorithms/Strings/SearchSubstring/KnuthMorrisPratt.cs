﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Strings.SearchSubstring
{
    public class KnuthMorrisPratt : ISearchSubstring, ISearchPatternInStream
    {
        int[][] dfa;
        string _pattern = string.Empty;
        int R = SearchSubstring.RadixUTF;

        public int Search(string pattern, string str)
        {
            if (_pattern != pattern)
                ConstructDfa(pattern);
            int i, j, N = str.Length, M = pattern.Length;
            for (i = 0, j = 0; i < N && j < M; i++)
                j = dfa[str[i]][j];
            if (j == M) return i - M;
            else return SearchSubstring.NOT_FOUND;
        }

        private void ConstructDfa(string pat)
        {
            _pattern = pat;
            int M = pat.Length;
            dfa = new int[R][];
            for (int i = 0; i < R; i++)
            {
                dfa[i] = new int[M];
            }

            dfa[pat[0]][0] = 1;
            for (int X = 0, j = 1; j < M; j++)
            {
                for (int c = 0; c < R; c++)
                    dfa[c][j] = dfa[c][X];
                dfa[pat[j]][j] = j + 1;
                
                X = dfa[pat[j]][X];
            }
        }

        public int Search(string pattern, System.IO.StreamReader input)
        {
            int i, j, M = pattern.Length;
            for (i = 0, j = 0; !input.EndOfStream && j < M; i++)
                j = dfa[input.Read()][j];
            if (j == M) return i - M;
            else return SearchSubstring.NOT_FOUND;
        }
    }
}
