﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Strings.SearchSubstring
{
    public class BruteForce : ISearchSubstring
    {
        // worst case M * N
        public int Search(string pattern, string str)
        {
            int M = pattern.Length;
            int N = str.Length;
            for (int i = 0; i < N - M; i++)
            {
                int j;
                for (j = 0; j < M; j++)
                {
                    if (str[i + j] != pattern[j])
                        break;
                }
                if (j == M) return i;
            }
            return SearchSubstring.NOT_FOUND;
        }
    }

    public class BruteForce2 : ISearchSubstring
    {
        // worst case M * N
        public int Search(string pattern, string str)
        {
            int M = pattern.Length;
            int N = str.Length;
            int i, j;
            for (i = 0, j = 0; i < N && j < M; i++)
            {
                if (str[i] == pattern[j]) j++;
                else
                {
                    i -= j;
                    j = 0;
                }
            }
            if (j == M) return i - M;
            else return SearchSubstring.NOT_FOUND;
        }
    }
}
