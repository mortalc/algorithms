﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Strings.SearchSubstring
{
    interface ISearchPatternInStream
    {
        int Search(string pattern, StreamReader input);
    }
}
