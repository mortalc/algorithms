﻿using System;

namespace Utilities.Algorithms.Strings
{
    public static class LongestCommonPrefix
    {
        public static int Lcp(this string s, string another)
        {
            int i = 0;
            int len = Math.Min(s.Length, another.Length);
            for (; i < len; i++)
                if (s[i] != another[i])
                    break;
            return i;
        }
    }
}
