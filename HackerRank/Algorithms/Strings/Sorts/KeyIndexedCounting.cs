﻿namespace Utilities.Algorithms.Strings.Sorts
{
    public class KeyIndexedCounting
    {
        public static string Sort(string s)
        {
            int len = s.Length;
            int R = 65526; // radix of unicode
            //char[] charS = s.ToCharArray();
            char[] aux = new char[len];

            int[] count = new int[R + 1];

            for (int i = 0; i < len; i++)
                //count[(int)s[i] + 1]++;
            count[s.CharAt(i) + 1]++;

            for (int r = 0; r < R; r++)
                count[r + 1] += count[r];// подсуммы

            for (int i = 0; i < len; i++)
                //aux[count[(int)s[i]]++] = s[i];// расставление по местам
                aux[count[s.CharAt(i)]++] = s[i];// расставление по местам

            //for (int i = 0; i < len; i++)
            //    charS[i] = aux[i];

            //return new string(charS);
            return new string(aux);
        }
    }
}
