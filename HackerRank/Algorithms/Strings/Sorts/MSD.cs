﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Algorithms.Sorting;

namespace Utilities.Algorithms.Strings.Sorts
{
    // most significant digit first sort
    public class MSD
    {
        static int R = 65526; // radix of unicode

        public static void Sort(string[] a)
        {
            string[] aux = new string[a.Length];
            Sort(a, aux, 0, a.Length - 1, 0);
        }

        private static bool Less(string v, string w, int d)
        {
            //if (v.Length <= d) return false;
            //if (w.Length <= d) return true;
            return v.Substring(d).CompareTo(w.Substring(d)) < 0;
        }
        
        private static void Sort(string[] a, string[] aux, int lo, int hi, int d)
        {
            if (hi <= lo) return;

            if (hi - lo < 8)
            {
                // insertion sort for small arrays
                for (int i = lo; i <= hi; i++)
                    for (int j = i; j > lo && Less(a[j], a[j - 1], d); j--)
                        SortHelpers.Exch(a, j, j - 1);
                return;
            }

            // KeyIndexedCounting
            int[] count = new int[R + 2];

            for (int i = lo; i <= hi; i++)
                count[MSD.CharAt(a[i], d) + 2]++;// частота встречания

            for (int r = 0; r < R + 1; r++)
                count[r + 1] += count[r];// подсуммы

            for (int i = lo; i <= hi; i++)
                aux[count[MSD.CharAt(a[i],d) + 1]++] = a[i];

            for (int i = lo; i <= hi; i++)
                a[i] = aux[i - lo];

            // sort R subarrays recursively
            for (int r = 0; r < R; r++)
                Sort(a, aux, lo + count[r], lo + count[r + 1] - 1, d + 1);
        }

        private static int CharAt(string s, int d)
        {
            if (d < s.Length) return char.ConvertToUtf32(s, d);
            else return -1;
        }
    }

    public static partial class StringExtensions
    {
        public static int CharAt(this string s, int d)
        {
            return char.ConvertToUtf32(s, d);
        }
    }
}
