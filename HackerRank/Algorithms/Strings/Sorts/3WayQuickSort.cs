﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Algorithms.Sorting;

namespace Utilities.Algorithms.Strings.Sorts
{
    public class ThreeWayQuickSort
    {
        public static void Sort(string[] a)
        {
            Sort(a, 0, a.Length - 1, 0);
        }

        private static void Sort(string[] a, int lo, int hi, int d)
        {
            if (hi <= lo) return;
            int lt = lo, gt = hi;
            int v = a[lt].CharAt(d);
            int i = lo + 1;
            while (i <= gt)
            {
                int t = a[i].CharAt(d);
                if (t < v) SortHelpers.Exch(a, lt++, i++);
                else if (t > v) SortHelpers.Exch(a, i, gt--);
                else i++;
            }

            Sort(a, lo, lt - 1, d);
            if (v >= 0) Sort(a, lt, gt, d + 1);
            Sort(a, gt + 1, hi, d);
        }
    }
}
