﻿using System;

namespace Utilities.Algorithms.Strings.Sorts
{
    public class LongestRepeatedSubstring
    {
        public static string Lrs(string s)
        {
            int N = s.Length;
            string[] suffixes = new string[N];
            for (int i = 0; i < N; i++)
                suffixes[i] = s.Substring(i);

            Array.Sort(suffixes);

            string lrs = string.Empty;
            for (int i = 0; i < N - 1; i++)
            {
                int len = suffixes[i].Lcp(suffixes[i + 1]);
                if (len > lrs.Length)
                    lrs = suffixes[i].Substring(0, len);
            }
            return lrs;
        }

        public static string Lrs(string s1, string s2)
        {
            int N = s1.Length;
            string[] suffixes = new string[N];
            for (int i = 0; i < N; i++)
                suffixes[i] = s1.Substring(i);

            int N2 = s2.Length;
            string[] suffixes2 = new string[N2];
            for (int i = 0; i < N2; i++)
                suffixes2[i] = s2.Substring(i);

            Array.Sort(suffixes);
            Array.Sort(suffixes2);

            string lrs = string.Empty;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N2; j++)
                {
                    int len = suffixes[i].Lcp(suffixes2[j]);
                    if (len > lrs.Length)
                        lrs = suffixes[i].Substring(0, len);
                }
            }
            return lrs;
        }
    }
}
