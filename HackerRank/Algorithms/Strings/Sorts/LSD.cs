﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Strings.Sorts
{
    public class LSD
    {
        // least significant digit first sort
        public static void Sort(string[] a, int lengthOfStrings)
        {
            int len = a.Length;
            int R = 65526; // radix of unicode
            string[] aux = new string[len];

            for (int d = lengthOfStrings - 1; d >= 0; d--)
            {
                int[] count = new int[R + 1];

                for (int i = 0; i < len; i++)
                    count[a[i].CharAt(d) + 1]++;

                for (int r = 0; r < R; r++)
                    count[r + 1] += count[r];// подсуммы

                for (int i = 0; i < len; i++)
                    aux[count[a[i].CharAt(d)]++] = a[i];

                for (int i = 0; i < len; i++)
                    a[i] = aux[i];
            }
            //return a;
        }
    }
}
