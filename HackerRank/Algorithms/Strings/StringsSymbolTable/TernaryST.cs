﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Algorithms.Strings.StringsSymbolTable
{
    // TST
    public class TernaryST<Value> : IStringST<Value>
    {
        Node root;

        public IEnumerable<string> Keys
        {
            get
            {
                Queue<string> q = new Queue<string>();
                Collect(root, new StringBuilder(), q);
                return q;
            }
        }

        private void Collect(Node x, StringBuilder prefix, Queue<string> q)
        {
            if (x == null) return;

            Collect(x.Left, prefix, q);

            if (x.Val != null) q.Enqueue(prefix.ToString() + x.Character);

            Collect(x.Mid, prefix.Append(x.Character), q);
            prefix.Remove(prefix.Length - 1, 1);

            Collect(x.Right, prefix, q);
        }

        public bool Contains(string key)
        {
            return Get(key) != null;
        }

        public void Delete(string key)
        {
            throw new NotImplementedException();
        }

        public Value Get(string key)
        {
            Node x = Get(root, key, 0);
            if (x == null) return default(Value);
            return x.Val;
        }

        Node Get(Node x, string key, int d)
        {
            if (x == null) return null;
            char c = key[d];
            if (c < x.Character) return Get(x.Left, key, d);
            else if (c > x.Character) return Get(x.Right, key, d);
            else if (d < key.Length - 1) return Get(x.Mid, key, d + 1);
            else return x;
        }

        public void Put(string key, Value val)
        {
            root = Put(root, key, val, 0);
        }

        Node Put(Node x, string key, Value val, int d)
        {
            char c = key[d];
            if (x == null) { x = new Node(); x.Character = c; }
            if (c < x.Character) x.Left = Put(x.Left, key, val, d);
            else if (c > x.Character) x.Right = Put(x.Right, key, val, d);
            else if (d < key.Length - 1) x.Mid = Put(x.Mid, key, val, d + 1);
            else x.Val = val;
            return x;
        }

        public IEnumerable<string> KeysWithPrefix(string prefix)
        {
            Queue<String> queue = new Queue<String>();
            Node x = Get(root, prefix, 0);
            if (x == null) return queue;
            if (x.Val != null) queue.Enqueue(prefix);
            Collect(x.Mid, new StringBuilder(prefix), queue);
            return queue;
        }

        public IEnumerable<string> KeysThatMatch(string pattern)
        {
            Queue<String> queue = new Queue<String>();
            Collect(root, new StringBuilder(), 0, pattern, queue);
            return queue;
        }

        private void Collect(Node x, StringBuilder prefix, int i, String pattern, Queue<String> queue)
        {
            if (x == null) return;
            char c = pattern[i];
            if (c == '.' || c < x.Character) Collect(x.Left, prefix, i, pattern, queue);
            if (c == '.' || c == x.Character)
            {
                if (i == pattern.Length - 1 && x.Val != null) queue.Enqueue(prefix.ToString() + x.Character);
                if (i < pattern.Length - 1)
                {
                    Collect(x.Mid, prefix.Append(x.Character), i + 1, pattern, queue);
                    prefix.Remove(prefix.Length - 1, 1);
                }
            }
            if (c == '.' || c > x.Character) Collect(x.Right, prefix, i, pattern, queue);
        }

        public string LongestPrefixOf(string query)
        {
            if (query == null || query.Length == 0) return null;
            int length = 0;
            Node x = root;
            int i = 0;
            while (x != null && i < query.Length)
            {
                char c = query[i];
                if (c < x.Character) x = x.Left;
                else if (c > x.Character) x = x.Right;
                else
                {
                    i++;
                    if (x.Val != null) length = i;
                    x = x.Mid;
                }
            }
            return query.Substring(0, length);
        }

        private class Node
        {
            public Value Val { get; set; }
            public char Character { get; set; }
            public Node Left { get; set; }
            public Node Mid { get; set; }
            public Node Right { get; set; }
        }
    }
}
