﻿using System.Collections.Generic;

namespace Utilities.Algorithms.Strings.StringsSymbolTable
{
    public interface IStringST<Value>
    {
        void Put(string key, Value val);
        Value Get(string key);
        void Delete(string key);
        bool Contains(string key);
        IEnumerable<string> Keys { get; }
        IEnumerable<string> KeysWithPrefix(string prefix);
        IEnumerable<string> KeysThatMatch(string query);
        string LongestPrefixOf(string prefix);
    }
}
