﻿namespace Utilities.Algorithms.Helpers
{
    public class StackArr<T>
    {
        T[] arr = null;
        int last = -1;
        int N = 0;

        public StackArr()
        {

        }

        public bool IsEmpty()
        {
            return last < 0;
        }

        public T Pop()
        {
            T val = arr[last];
            if (--last < N / 4)
                HelperCommon.HalfArray<T>(ref arr, ref N);
            return val;
        }

        public void Push(T val)
        {
            if (++last >= N)
            {
                HelperCommon.DoubleArray<T>(ref arr, ref N);
            }
            if (arr.Length == N)
            {
                arr[last] = val;
            }
        }

    //    private void DoubleArray()
    //    {
    //        int oldN = N;
    //        N *= 2;
    //        if (N == 0) N = 1;
    //        if (N > 0)
    //        {
    //            T[] newArr = new T[N];
    //            for (int i = 0; i < oldN; i++)
    //                newArr[i] = arr[i];
    //            arr = newArr;
    //        }
    //    }

    //    private void HalfArray()
    //    {
    //        N /= 2;
    //        if (N > 0)
    //        {
    //            T[] newArr = new T[N];
    //            for (int i = 0; i < N; i++)
    //                newArr[i] = arr[i];
    //            arr = newArr;
    //        }
    //    }
    }

    public class StackList<T>
    {
        Item first = null;

        private class Item
        {
            public T Val;
            public Item Next;
        }

        public bool IsEmpty()
        {
            return first == null;
        }

        public T Pop()
        {
            T val = default(T);
            if (first != null)
            {
                val = first.Val;
                first = first.Next;
            }
            return val;
        }

        public void Push(T val)
        {
            Item item = new Item();
            item.Val = val;
            item.Next = first;
            first = item;
        }
    }
}