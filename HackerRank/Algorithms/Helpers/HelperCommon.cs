﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Helpers
{
    public class HelperCommon
    {
        public static void DoubleArray<T>(ref T[] a, ref int N)
        {
            int oldN = N;
            N *= 2;
            if (N == 0) N = 1;
            //if (a == null || a.Length == 0)
            //    a = new T[N];
            //if (N > 0)
            {
                T[] newArr = new T[N];
                for (int i = 0; i < oldN; i++)
                    newArr[i] = a[i];
                a = newArr;
            }
        }

        public static void DoubleArray<T>(ref T[] a, int N)
        {
            int oldN = N;
            N *= 2;
            if (N == 0) N = 1;
            if (a == null || a.Length == 0)
                a = new T[N];
            //if (N > 0)
            {
                T[] newArr = new T[N];
                for (int i = 0; i < oldN; i++)
                    newArr[i] = a[i];
                a = newArr;
            }
        }

        public static void HalfArray<T>(ref T[] a, ref int N)
        {
            N /= 2;
            if (N > 0)
            {
                T[] newArr = new T[N];
                for (int i = 0; i < N; i++)
                    newArr[i] = a[i];
                a = newArr;
            }
        }

        public static void HalfArray<T>(ref T[] a, int N)
        {
            N /= 2;
            if (N > 0)
            {
                T[] newArr = new T[N];
                for (int i = 0; i < N; i++)
                    newArr[i] = a[i];
                a = newArr;
            }
        }

    }
}
