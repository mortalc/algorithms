﻿using System;
using System.Collections.Generic;

namespace Utilities.Algorithms.Helpers
{
    public class IndexedPriorityQueue<Key> where Key : IComparable<Key>
    {
        protected int N = 0; // количество элементов очереди
        protected int[] pq; // бинамиальная пирамида с индексацией с 1
        protected int maxItems = 0;
        protected int[] qp;  // qp[pq[i]] == pq[qp[i]] == i
        protected Key[] keys;


        public IndexedPriorityQueue(int maxN)
        {
            // емкость MaxN, индексы 0 - MaxN-1
            maxItems = maxN + 1;
            keys = new Key[maxN + 1];
            pq = new int[maxN + 1];
            qp = new int[maxN + 1];
            for (int i = 0; i <= maxN; i++)
                qp[i] = -1;
        }

        // вставка элемента Key и связывание его с ииндексом k
        public void Insert(int k, Key key)
        {
            N++;
            qp[k] = N;
            pq[N] = k;
            keys[k] = key;
            Swim(N);
        }

        // замена элемента, связанного с k, на Key
        public void Change(int k, Key key)
        {
            keys[k] = key;
            Swim(qp[k]);
            Sink(qp[k]);
        }

        public virtual void DecreaseKey(int i, Key key)
        {

        }

        public virtual void IncreaseKey(int i, Key key)
        {

        }

        // связан ли индекс k  с каким-либо элементом
        public bool Contains(int k)
        {
            return qp[k] != -1;
        }

        // удаление k и связанного с ним элемента
        public void Delete(int k)
        {
            Exch(k, N--);
            Swim(qp[k]);
            Sink(qp[k]);
            keys[pq[N + 1]] = default(Key);
            qp[pq[N + 1]] = -1;
        }

        public Key Top()
        {
            return keys[pq[1]];
        }

        public int TopIndex()
        {
            return pq[1];
        }

        public int DeleteTop()
        {
            int indexOfTop = pq[1];
            Exch(1, N--);
            Sink(1);
            keys[pq[N + 1]] = default(Key);
            qp[pq[N + 1]] = -1;
            return indexOfTop;
        }

        public bool IsEmpty()
        {
            return N == 0;
        }

        public int Size()
        {
            return N;
        }

        protected void Swim(int k) // восходящий обход
        {
            while (k > 1 && Less(k / 2, k))
            {
                Exch(k / 2, k);
                k = k / 2;
            }
        }

        protected void Sink(int k) // нисходящее восстановление пирамидальности
        {
            while (2 * k <= N)
            {
                int j = k * 2;
                if (j < N && Less(j, j + 1)) j++;
                if (!Less(k, j)) break;
                Exch(j, k);
                k = j;
            }
        }

        protected virtual bool Less(int i, int j)
        {
            return pq[i].CompareTo(pq[j]) < 0;
        }

        protected void Exch(int i, int j)
        {
            int swap = pq[i];
            pq[i] = pq[j];
            pq[j] = swap;
            qp[pq[i]] = i;
            qp[pq[j]] = j;
        }
    }

    public class IndexedMaxPQ<Key> : IndexedPriorityQueue<Key> where Key : IComparable<Key>
    {
        public IndexedMaxPQ(int maxN) : base(maxN)
        {

        }

        public Key Max() // возврат макс значения
        {
            return Top();
        }

        public int DelMax() // возврат и удаление из очереди макс значения
        {
            return DeleteTop();
        }

        public override void DecreaseKey(int i, Key key)
        {
            if (i < 0 || i >= maxItems) throw new IndexOutOfRangeException();
            if (!Contains(i)) throw new KeyNotFoundException();
            if (keys[i].CompareTo(key) <= 0)
                throw new ArgumentException("Calling decreaseKey() with given argument would not strictly decrease the key");
            keys[i] = key;
            Sink(qp[i]);
        }

        public override void IncreaseKey(int i, Key key)
        {
            if (i < 0 || i >= maxItems) throw new IndexOutOfRangeException();
            if (!Contains(i)) throw new KeyNotFoundException();
            if (keys[i].CompareTo(key) >= 0)
                throw new ArgumentException("Calling decreaseKey() with given argument would not strictly increase the key");
            keys[i] = key;
            Swim(qp[i]);
        }

        protected override bool Less(int i, int j)
        {
            return keys[pq[i]].CompareTo(keys[pq[j]]) < 0;
        }
    }


    public class IndexedMinPQ<Key> : IndexedPriorityQueue<Key> where Key : IComparable<Key>
    {
        public IndexedMinPQ(int maxN) : base(maxN)

        {

        }

        public Key Min() // возврат макс значения
        {
            return Top();
        }

        public int DelMin() // возврат и удаление из очереди макс значения
        {
            return DeleteTop();
        }

        public override void DecreaseKey(int i, Key key)
        {
            if (i < 0 || i >= maxItems) throw new IndexOutOfRangeException();
            if (!Contains(i)) throw new KeyNotFoundException();
            if (keys[i].CompareTo(key) <= 0)
                throw new ArgumentException("Calling decreaseKey() with given argument would not strictly decrease the key");
            keys[i] = key;
            Swim(qp[i]);
        }

        public override void IncreaseKey(int i, Key key)
        {
            if (i < 0 || i >= maxItems) throw new IndexOutOfRangeException();
            if (!Contains(i)) throw new KeyNotFoundException();
            if (keys[i].CompareTo(key) >= 0)
                throw new ArgumentException("Calling decreaseKey() with given argument would not strictly increase the key");
            keys[i] = key;
            Sink(qp[i]);
        }

        protected override bool Less(int i, int j)
        {
            return More(i, j);
        }

        protected bool More(int i, int j)
        {
            return keys[pq[i]].CompareTo(keys[pq[j]]) > 0;
        }
    }

}
