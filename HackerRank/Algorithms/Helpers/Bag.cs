﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Helpers
{
    public class Bag<Item> : IEnumerable<Item>
    {
        private Node<Item> _first;    // beginning of bag
        private int _N;               // number of elements in bag

        private class Node<Item>
        {
            public Item Value { get; set; }
            public Node<Item> Next { get; set; }

            public Node()
            {
                // TODO: Complete member initialization
            }
        }

        public Bag()
        {
            _first = null;
            _N = 0;
        }

        public int Size()
        {
            return _N;
        }

        public bool IsEmpty()
        {
            return _first == null;
        }

        public void Add(Item item)
        {
            Node<Item> n = new Node<Item>() { Value = item };
            n.Next = _first;
            _first = n;
            _N++;
        }

        public void Remove(Item item)
        {
            Node<Item> n = new Node<Item>() { Value = item };

            Node<Item> prev = _first;
            if (prev.Value.Equals(item))
            {
                _first = _first.Next;
                prev = null;
                _N--;
                return;
            }

            while (prev != null)
            {
                Node<Item> curr = prev.Next;
                if (curr != null && curr.Value.Equals(item))
                {
                    prev.Next = curr.Next;
                    curr.Next = null;
                    curr = null;
                    _N--;
                }
                
                prev = prev.Next;
            }

        }

        public IEnumerator<Item> GetEnumerator()
        {
            return new ListEnumerator<Item>(_first);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private class ListEnumerator<Item> : IEnumerator<Item>
        {
            private Node<Item> _curr;
            private Node<Item> _first;

            public ListEnumerator(Node<Item> first)
            {
                //_curr = first;
                _first = first;
            }

            public Item Current
            {
                get { return _curr.Value; }
            }

            public void Dispose()
            {
                _curr = null;
                _first = null;
            }

            object System.Collections.IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                if (_curr == null)
                    _curr = _first;
                else
                    _curr = _curr.Next;
                return _curr != null;
            }

            public void Reset()
            {
                _curr = _first;
            }
        }

    }
}
