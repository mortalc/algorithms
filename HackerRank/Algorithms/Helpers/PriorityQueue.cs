﻿using System;

namespace Utilities.Algorithms.Helpers
{
    public abstract class PriorityQueue<Key> where Key : IComparable<Key>
    {
        protected int N = 0;
        protected Key[] pq;
        protected int maxItems = 0;

        public PriorityQueue() { }
        public PriorityQueue(int maxN) // создание очереди с приоритетами с начальным объектом max
        {
            maxItems = maxN + 1;
            pq = new Key[maxItems];
        }
        public PriorityQueue(Key[] a) // создание очереди с приоритетами из массива ключей
        { }

        public void insert(Key v)
        {
            if ((N == 0 || N + 1 == pq.Length) && maxItems == 0)
                HelperCommon.DoubleArray<Key>(ref pq, N + 1);
            pq[++N] = v;
            swim(N);
        }

        protected void swim(int k) // восходящий обход
        {
            while (k > 1 && less(k / 2, k))
            {
                exch(k / 2, k);
                k = k / 2;
            }
        }

        protected void sink(int k) // нисходящее восстановление пирамидальности
        {
            while (2 * k <= N)
            {
                int j = k * 2;
                if (j < N && less(j, j + 1)) j++;
                if (!less(k, j)) break;
                exch(j, k);
                k = j;
            }
        }

        protected virtual bool less(int i, int j)
        {
            return pq[i].CompareTo(pq[j]) < 0;
        }

        public Key delTop()
        {
            Key max = pq[1];
            exch(1, N--);
            pq[N + 1] = default(Key);
            sink(1);
            if((N + 1 < pq.Length / 4) && maxItems == 0)
                HelperCommon.HalfArray<Key>(ref pq, N + 1);
            return max;
        }

        public Key top()
        {
            return pq[1];
        }

        public bool isEmpty() { return N == 0; }
        public int size() { return N; }
        protected void exch(int i, int j)
        {
            Key t = pq[i];
            pq[i] = pq[j];
            pq[j] = t;
        }
    }

    public class MaxPQ<Key> : PriorityQueue<Key> where Key : IComparable<Key>
    {

        public MaxPQ() { }
        public MaxPQ(int max)  // создание очереди с приоритетами с начальным объектом max
            : base(max)
        { }
        public MaxPQ(Key[] a) // создание очереди с приоритетами из массива ключей
            : base(a)
        { }

        public Key max() // возврат макс значения
        {
            return top();
        }


        public Key delMax() // возврат и удаление из очереди макс значения
        {
            return delTop();
        }
    }

    public class MinPQ<Key> : PriorityQueue<Key> where Key : IComparable<Key>
    {

        public MinPQ() { }
        public MinPQ(int max)  // создание очереди с приоритетами с начальным объектом max
            : base(max)
        { }
        public MinPQ(Key[] a) // создание очереди с приоритетами из массива ключей
            : base(a)
        { }

        protected override bool less(int i, int j)
        {
            return more(i, j);
        }

        protected bool more(int i, int j)
        {
            return pq[i].CompareTo(pq[j]) > 0;
        }

        public Key min() // возврат мин значения
        {
            return top();
        }


        public Key delMin() // возврат и удаление из очереди макс значения
        {
            return delTop();
        }
    }
}
