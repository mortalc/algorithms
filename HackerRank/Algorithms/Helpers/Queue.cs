﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Helpers
{
    //public class QueueArr<T>
    //{
    //    T[] arr = null;
    //    int last = -1;
    //    int N = 0;

    //    public StackArr()
    //    {

    //    }

    //    public bool IsEmpty()
    //    {
    //        return last < 0;
    //    }

    //    public T Enqueue()
    //    {
    //        T val = arr[last];
    //        if (--last < N / 4)
    //            HelperCommon.HalfArray<T>(ref arr, ref N);
    //        return val;
    //    }

    //    public void Deque(T val)
    //    {
    //        if (++last >= N)
    //        {
    //            HelperCommon.DoubleArray<T>(ref arr, ref N);
    //        }
    //        if (arr.Length == N)
    //        {
    //            arr[last] = val;
    //        }
    //    }

    //}


    public class Queue<T>
    {
        Item first = null;
        Item last = null;

        private class Item
        {
            public T Val;
            public Item Next;
        }

        public bool IsEmpty()
        {
            return first == null;
        }

        public T Dequeue()
        {
            T val = default(T);
            if (first != null)
            {
                val = first.Val;
                first = first.Next;
            }
            return val;
        }

        public void Enqueue(T val)
        {
            Item item = new Item();
            item.Val = val;
            item.Next = null;
            if (!IsEmpty())
            {
                last.Next = item;    
            }            
            last = item;
            if (IsEmpty())
            {
                first = last;
            }
        }
    }
}
