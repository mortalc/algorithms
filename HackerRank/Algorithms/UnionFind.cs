﻿namespace Utilities.Algorithms
{
    public class UnionFind
    {
        int[] arr;
        int[] sz;
        int count = 0;

        public UnionFind(int N)
        {
            count = N;
            arr = new int[N];
            sz = new int[N];
            for (int i = 0; i < N; i++)
            {
                arr[i] = i;
                sz[i] = 1;
            }
        }

        public void Union(int p, int q)
        {
            int pRoot = Root(p);
            int qRoot = Root(q);
            if (pRoot == qRoot)
                return;
            // меньший корень (размер дерева) указывает на больший
            if (sz[pRoot] < sz[qRoot])
            {
                arr[pRoot] = qRoot;
                sz[qRoot] += sz[pRoot];
            }
            else
            {
                arr[qRoot] = pRoot;
                sz[pRoot] += sz[qRoot];
            }

            count--;
        }


        public bool IsConnected(int p, int q)
        {
            return Root(p) == Root(q);
        }

        public int Root(int p)
        {
            while (p != arr[p])
            {
                arr[p] = arr[arr[p]];    // path compression by halving
                p = arr[p];
            }
            return p;
        }

        public int Count()
        {
            return count;
        }

        public int[] GetArr()
        {
            return arr;
        }

        public int GetSize(int pRoot)
        {
            return sz[pRoot];
        }

        public bool IsRoot(int p)
        {
            return p == arr[p];
        }
    }
}
