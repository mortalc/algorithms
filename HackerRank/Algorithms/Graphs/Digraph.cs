﻿using System.IO;

namespace Utilities.Algorithms.Graphs
{
    public class Digraph : Graph
    {
        //private int v;
        //private int e;
        //private Bag<int>[] adj;

        public Digraph() // create an empty graph with V verticies
        {

        }

        public Digraph(int V) // create an empty graph with V verticies
            : base(V)
        {
            //this.v = V;
            //adj = new Bag<int>[v];
            //for (int i = 0; i < v; i++)
            //    adj[i] = new Bag<int>();
        }

        public Digraph(StreamReader input) // create from input stream
            : base(input)
        {
            //this.v = int.Parse(input.ReadLine());
            //int edges = int.Parse(input.ReadLine());
            //for (; edges > 0; edges--)
            //{
            //    string[] vert = input.ReadLine().Split(' ');
            //    int v1 = int.Parse(vert[0]);
            //    int v2 = int.Parse(vert[1]);
            //    AddEdge(v1, v2);
            //}
        }

        public override void AddEdge(int v, int w) // add a directed v->w
        {
            adj[v].Add(w);
            e++;
        }

        //public IEnumerable<int> Adj(int v)
        //{
        //    return adj[v];
        //}

        //public int V() // number of verticies
        //{
        //    return v;
        //}

        //public int E() // number of edges
        //{
        //    return e;
        //}

        public Digraph Reverse() // reverse of this Digraph
        {
            Digraph d = new Digraph(this.v);
            for (int s = 0; s < this.v; s++)
            {
                foreach (var w in this.Adj(v))
                    d.AddEdge(w, v);
            }
            return d;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
