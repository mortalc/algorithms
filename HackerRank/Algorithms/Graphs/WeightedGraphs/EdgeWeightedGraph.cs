﻿using System.Collections.Generic;
using System.IO;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class EdgeWeightedGraph
    {
        protected int v, e;
        protected Bag<Edge>[] adj;

        public EdgeWeightedGraph(int V)
        {
            this.v = V;
            adj = new Bag<Edge>[V];
            for (int i = 0; i < V; i++)
            {
                adj[i] = new Bag<Edge>();
            }
        }

        public EdgeWeightedGraph(StreamReader input)
        {
            this.v = int.Parse(input.ReadLine());
            int edges = int.Parse(input.ReadLine());
            for (; edges > 0; edges--)
            {
                string[] vert = input.ReadLine().Split(' ');
                int v1 = int.Parse(vert[0]);
                int v2 = int.Parse(vert[1]);
                int weight = int.Parse(vert[2]);
                AddEdge(new Edge(v1, v2, weight));
            }
        }

        public virtual void AddEdge(Edge edge)
        {
            int v = edge.Either(), w = edge.Other(v);
            adj[v].Add(edge);
            adj[w].Add(edge);
            e++;
        }

        public virtual IEnumerable<Edge> Adj(int v)
        {
            return adj[v];
        }

        public IEnumerable<Edge> Edges()
        {
            Bag<Edge> bag = new Bag<Edge>();
            for (int i = 0; i < this.e; i++)
            {
                foreach (var item in adj[i])
                    bag.Add(item);
            }
            return bag;
        }

        public int V() // VerteciesCount()
        {
            return v;
        }

        public int E() //EdgesCount
        {
            return e;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
