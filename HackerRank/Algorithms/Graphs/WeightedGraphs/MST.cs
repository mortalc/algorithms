﻿using System.Collections.Generic;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class MST // Minimum Spanning Tree
    {
        public MST(EdgeWeightedGraph G)
        {

        }

        public virtual IEnumerable<Edge> Edges()
        {
            return new Bag<Edge>();
        }

        public virtual double Weight()
        {
            return 0.0;
        }

    }
}
