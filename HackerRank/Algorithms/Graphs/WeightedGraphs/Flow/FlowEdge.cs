﻿using System;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Flow
{
    public class FlowEdge : DirectedEdge
    {
        private double flow = 0.0;
        private double capacity;

        public FlowEdge(int v, int w, double capacity) : base(v, w, 0)
        {
            this.capacity = capacity;
        }

        public override int Other(int v)
        {
            if (v == From)
                return To;
            else if (v == To)
                return From;
            else throw new System.Exception("Illegal endpoint");
        }

        public double Capacity { get { return capacity; } }

        public double Flow { get { return flow; } }

        // residual capacity toward v 
        public double ResidualCapacityTo(int v)
        {
            if (v == From) return flow;   // backward edge
            else if (v == To) return Capacity - flow; // forward edge
            else throw new ArgumentException();
        }

        public void AddResidualCapacity(int v, double delta)
        {
            if (v == From) flow -= delta; // backward edge
            else if (v == To) flow += delta; // forward edge
            else throw new ArgumentException();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
