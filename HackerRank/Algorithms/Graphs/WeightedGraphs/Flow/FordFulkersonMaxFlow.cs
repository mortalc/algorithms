﻿using System;
using System.Collections.Generic;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Flow
{
    public class FordFulkersonMaxFlow
    {
        bool[] marked;
        FlowEdge[] edgeTo; // last edge on s->v path
        double value; // value of flow

        public FordFulkersonMaxFlow(FlowNetwork G, int s, int t)
        {
            value = 0.0;
            while (HasAugmentingPath(G, s, t))
            {
                double bottle = double.MaxValue;
                for (int v = t; v != s; v = edgeTo[v].Other(v)) // compute bottleneck capacity
                    bottle = Math.Min(bottle, edgeTo[v].ResidualCapacityTo(v));

                for (int v = t; v != s; v = edgeTo[v].Other(v)) // residual flow
                    edgeTo[v].AddResidualCapacity(v, bottle);

                value += bottle;
            }
        }

        private bool HasAugmentingPath(FlowNetwork G, int s, int t)
        {
            edgeTo = new FlowEdge[G.V];
            marked = new bool[G.V];

            Queue<int> queue = new Queue<int>();

            queue.Enqueue(s);

            marked[s] = true;
            while (queue.Count > 0)
            {
                int v = queue.Dequeue();
                foreach (var e in G.Adj(v))
                {
                    int w = e.Other(v);
                    // found path from s to w in the residual network
                    if (e.ResidualCapacityTo(w) > 0 && !marked[w])
                    {
                        edgeTo[w] = e; // save last edge on path to w
                        marked[w] = true; // mark w
                        queue.Enqueue(w); // add w to the queue
                    }
                }
            }

            return marked[t];
        }

        public double Value()
        {
            return value;
        }

        // is v reachable from s in residual network
        public bool inCut(int v)
        {
            return marked[v];
        }
    }
}
