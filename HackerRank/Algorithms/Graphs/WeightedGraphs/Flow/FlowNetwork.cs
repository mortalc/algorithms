﻿using System.Collections.Generic;
using System.IO;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Flow
{
    public class FlowNetwork //: EdgeWeightedGraph
    {
        private int _v;
        public int V { get {return _v;} }
        private Bag<FlowEdge>[] adj;

        public FlowNetwork(int V)
        {
            _v = V;
            adj = new Bag<FlowEdge>[V];
            for (int i = 0; i < V; i++)
                adj[i] = new Bag<FlowEdge>();
        }

        public FlowNetwork(StreamReader input)
        {
            _v = int.Parse(input.ReadLine());
            int edges = int.Parse(input.ReadLine());
            for (; edges > 0; edges--)
            {
                string[] vert = input.ReadLine().Split(' ');
                int v1 = int.Parse(vert[0]);
                int v2 = int.Parse(vert[1]);
                int weight = int.Parse(vert[2]);
                AddEdge(new FlowEdge(v1, v2, weight));
            }
        }



        public void AddEdge(FlowEdge e)
        {
            int v = e.From;
            int w = e.To;
            adj[v].Add(e);
            adj[w].Add(e);
        }

        public IEnumerable<FlowEdge> Adj(int v)
        {
            return adj[v];
        }
    }
}
