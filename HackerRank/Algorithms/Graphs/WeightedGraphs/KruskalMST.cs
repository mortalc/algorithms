﻿using System.Collections.Generic;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class KruskalMST : MST
    {
        private System.Collections.Generic.Queue<Edge> mst = new System.Collections.Generic.Queue<Edge>();

        public KruskalMST(EdgeWeightedGraph G) : base(G)
        {
            var pq = new MinPQ<Edge>();

            foreach (var edge in G.Edges())
                pq.insert(edge);

            UnionFind uf = new UnionFind(G.V());
            while (!pq.isEmpty() && mst.Count < G.V() - 1)
            {
                Edge edge = pq.delMin();
                int v = edge.Either();
                int w = edge.Other(v);
                if (!uf.IsConnected(v, w))
                {
                    mst.Enqueue(edge);
                    uf.Union(v, w);
                }
            }
        }

        public override IEnumerable<Edge> Edges()
        {
            return mst;
        }
    }
}
