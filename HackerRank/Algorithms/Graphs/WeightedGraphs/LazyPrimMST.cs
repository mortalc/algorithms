﻿using System.Collections.Generic;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class LazyPrimMST : MST
    {
        private bool[] marked;
        private System.Collections.Generic.Queue<Edge> mst;
        private MinPQ<Edge> pq;

        public LazyPrimMST(EdgeWeightedGraph G) : base(G)
        {
            pq = new MinPQ<Edge>();
            mst = new System.Collections.Generic.Queue<Edge>();
            marked = new bool[G.V()];
            Visit(G, 0);

            while (!pq.isEmpty())
            {
                Edge edge = pq.delMin();
                int v = edge.Either();
                int w = edge.Other(v);
                if (marked[v] && marked[w]) continue;
                mst.Enqueue(edge);
                if (!marked[v]) Visit(G, v);
                if (!marked[w]) Visit(G, w);
            }
        }

        private void Visit(EdgeWeightedGraph G, int v)
        {
            marked[v] = true;
            foreach (var edge in G.Adj(v))
                if (!marked[edge.Other(v)])
                    pq.insert(edge);
        }

        public override IEnumerable<Edge> Edges()
        {
            return mst;
        }
    }
}
