﻿using System;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    // weighted edge
    public class Edge : IComparable<Edge>
    {
        protected int v, w;
        protected double weight;

        public Edge(int v, int w, double weight)
        {
            this.v = v;
            this.w = w;
            this.weight = weight;
        }


        // usage: edge e,  v = e.Either; w = e.Other(v)
        public int Either()
        {
            return this.v;
        }

        public virtual int Other(int v)
        {
            if (this.v == v)
            {
                return w;
            }
            return v;
        }

        public int CompareTo(Edge other)
        {
            if (this.weight > other.weight) return 1;
            else if (this.weight < other.weight) return -1;
            return 0;
        }

        public double Weight { get { return this.weight; } }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
