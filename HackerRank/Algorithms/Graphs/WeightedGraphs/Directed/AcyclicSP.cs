﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Directed
{
    public class AcyclicSP : SP
    {
        public AcyclicSP(EdgeWeightedDigraph G, int from) : base(G, from)
        {
            for (int v = 0; v < G.V(); v++)
                distTo[v] = double.MaxValue;
            distTo[from] = 0.0;

            Topological topological = new Topological(G);
            foreach (var e in topological.Order())
                foreach (var v in G.Adj(e))
                    Relax(v);
        }
    }
}
