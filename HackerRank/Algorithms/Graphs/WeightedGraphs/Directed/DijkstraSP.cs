﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Directed
{
    public class DijkstraSP : SP
    {
        private IndexedMinPQ<double> pq;

        public DijkstraSP(EdgeWeightedDigraph G, int from) : base(G, from)
        {
            pq = new IndexedMinPQ<double>(G.V());
            for (int i = 0; i < G.V(); i++)
                distTo[i] = double.MaxValue;
            distTo[from] = 0.0;

            pq.Insert(from, 0.0);

            while (!pq.IsEmpty())
            {
                int v = pq.DelMin();
                foreach (var e in G.Adj(v))
                    Relax(e);
            }
        }

        protected override void Relax(DirectedEdge e)
        {
            int v = e.From, w = e.To;
            if (distTo[w] > distTo[v] + e.Weight)
            {
                distTo[w] = distTo[v] + e.Weight;
                edgeTo[w] = e;

                if (pq.Contains(w))
                    pq.DecreaseKey(w, distTo[w]);
                else
                    pq.Insert(w, distTo[w]);
            }
        }
    }
}
