﻿using System.Collections.Generic;
using System.IO;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class EdgeWeightedDigraph : Digraph
    {
        new private Bag<DirectedEdge>[] adj;
        //private int v;
        //private int e;

        public EdgeWeightedDigraph(int V) 
        {
            this.v = V;
            adj = new Bag<DirectedEdge>[V];
            for (int i = 0; i < V; i++)
            {
                adj[i] = new Bag<DirectedEdge>();
            }
        }

        public EdgeWeightedDigraph(StreamReader input)
        {
            this.v = int.Parse(input.ReadLine());
            int edges = int.Parse(input.ReadLine());
            for (; edges > 0; edges--)
            {
                string[] vert = input.ReadLine().Split(' ');
                int v1 = int.Parse(vert[0]);
                int v2 = int.Parse(vert[1]);
                int weight = int.Parse(vert[2]);
                AddEdge(new DirectedEdge(v1, v2, weight));
            }
        }

        public void AddEdge(DirectedEdge edge)
        {
            int v = edge.From, w = edge.To;
            adj[v].Add(edge);
            e++;
        }

        new public IEnumerable<DirectedEdge> Adj(int v)
        {
            return adj[v];
        }

        //public int V()
        //{
        //    return v;
        //}

        //public int E()
        //{
        //    return e;
        //}

        public IEnumerable<DirectedEdge> Edges()
        {
            Bag<DirectedEdge> bag = new Bag<DirectedEdge>();
            for (int i = 0; i < this.e; i++)
            {
                foreach (var item in adj[i])
                    bag.Add(item);
            }
            return bag;
        }
    }
}
