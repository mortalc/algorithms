﻿namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class DirectedEdge : Edge
    {
        public DirectedEdge(int from, int to, int weight)
            : base(from, to, weight)
        {
            //From = from;
            //To = to;
            //Weight = weight;
        }
        public int From { get { return v; } }
        public int To { get { return w; } }
        //public double Weight { get { return weight; } }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
