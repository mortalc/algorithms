﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Algorithms.Graphs.WeightedGraphs.Directed
{
    public class BellmanFordSP : SP
    {
        // no negative cycle, can be negative weights
        public BellmanFordSP(EdgeWeightedDigraph G, int from) : base(G, from)
        {
            for (int i = 0; i < G.V(); i++)
                for (int v = 0; v < G.V(); v++)
                    foreach (var e in G.Adj(v))
                        Relax(e);
        }
    }
}
