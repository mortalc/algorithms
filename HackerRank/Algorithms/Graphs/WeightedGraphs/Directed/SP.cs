﻿using System.Collections.Generic;

namespace Utilities.Algorithms.Graphs.WeightedGraphs
{
    public class SP // shortest path
    {
        protected double[] distTo;
        protected DirectedEdge[] edgeTo;

        public SP(EdgeWeightedDigraph G, int from)
        {
            distTo = new double[G.V()];
            edgeTo = new DirectedEdge[G.E()];
        }

        public double DistTo(int to)
        {
            return distTo[to];
        }

        public IEnumerable<DirectedEdge> PathTo(int to)
        {
            Stack<DirectedEdge> stack = new Stack<DirectedEdge>();
            for (DirectedEdge e = edgeTo[to]; e != null; e = edgeTo[e.From])
                stack.Push(e);
            return stack;
        }

        public bool HasPathTo(int to)
        {
            return true;
        }

        protected virtual void Relax(DirectedEdge e)
        {
            int v = e.From, w = e.To;
            if (distTo[w] > distTo[v] + e.Weight)
            {
                distTo[w] = distTo[v] + e.Weight;
                edgeTo[w] = e;
            }
        }

        public bool HasNegativeCycle()
        {
            return false;
        }

        public IEnumerable<DirectedEdge> NegativeCycle()
        {
            return null;
        }
    }
}
