﻿using System.Collections.Generic;

namespace Utilities.Algorithms.Graphs
{
    public class Cycle
    {
        private bool[] marked;
        private bool hasCycle;

        public Cycle(Graph G)
        {
            marked = new bool[G.V()];
            for (int s = 0; s < G.V(); s++)
            {
                if (!marked[s])
                    Dfs(G, s, s);
            }
        }

        private void Dfs(Graph G, int v, int u)
        {
            marked[v] = true;
            foreach (var w in G.Adj(v))
            {
                if (!marked[w])
                {
                    Dfs(G, w, v);
                }
                else if (w != u)
                {
                    hasCycle = true;
                }
            }
        }

        public bool HasCycle()
        {
            return hasCycle;
        }
    }

    public class DirectedCycle
    {
        private bool hasCycle = false;
        private int[] edgeTo;
        private bool[] marked;
        private bool[] onStack;
        private Stack<int> cycle;

        public DirectedCycle(Digraph G)
        {
            edgeTo = new int[G.V()];
            marked = new bool[G.V()];
            onStack = new bool[G.V()];
            for (int s = 0; s < G.V(); s++)
            {
                if (!marked[s])
                    Dfs(G, s);
            }
        }

        private void Dfs(Digraph G, int v)
        {
            onStack[v] = true;
            marked[v] = true;
            foreach (var w in G.Adj(v))
            {
                if (hasCycle) return;
                else if (!marked[w])
                {
                    edgeTo[w] = v;
                    Dfs(G, w);
                }
                else if (onStack[w])
                {
                    cycle = new Stack<int>();
                    for (int x = v; x != w; x = edgeTo[x])
                        cycle.Push(x);
                    cycle.Push(w);
                    cycle.Push(v);
                }
            }
            onStack[v] = false;
        }

        public bool HasCycle { get { return hasCycle; } }

        public IEnumerable<int> Cycle()
        {
            return cycle;
        }
    }
}
