﻿using System.Collections.Generic;

namespace Utilities.Algorithms.Graphs
{
    public class Paths
    {
        public Paths() // find graphs in G from source s
        {

        }

        public Paths(Graph G, int s) // find graphs in G from source s
        {

        }

        public virtual bool HasPathTo(int v) // is there a path from s to v
        {
            return true;
        }

        public virtual IEnumerable<int> PathTo(int v) // path from s to v; null if no path
        {
            return null;
        }
    }

    public class DepthFirstPaths : Paths
    {
        private bool[] marked; // marked[v] == true, if v connected to s
        private int[] edgeTo; // edgeTo[v] == previous vertex on path from s to v
        private int s;


        public DepthFirstPaths(Graph G, int s) : base(G, s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            edgeTo = new int[gCount];
            this.s = s;
            Dfs(G, s);
        }

        private void Dfs(Graph G, int v)
        {
            marked[v] = true;
            foreach (var d in G.Adj(v))
            {
                if (!marked[d])
                {
                    Dfs(G, d);
                    edgeTo[d] = v;
                }
            }
        }

        public override bool HasPathTo(int v)
        {
            return marked[v];
        }

        public override IEnumerable<int> PathTo(int v)
        {
            if (!HasPathTo(v)) return null;

            Stack<int> path = new Stack<int>();
            for (int i = v; i != s; i = edgeTo[i])
            {
                path.Push(i);
            }
            path.Push(s);
            return path;
        }
    }

    public class BreadthFirstPaths : Paths
    {
        private bool[] marked; // marked[v] == true, if v connected to s
        private int[] edgeTo; // edgeTo[v] == previous vertex on path from s to v
        private int[] distTo; // number of edges to vertex s
        private int s;

        public BreadthFirstPaths(Graph G, int s) : base(G, s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            edgeTo = new int[gCount];
            distTo = new int[gCount];
            distTo[s] = 0;
            this.s = s;
            Bfs(G, s);
        }

        private void Bfs(Graph G, int s)
        {
            Queue<int> q = new Queue<int>();
            q.Enqueue(s);
            marked[s] = true;
            while (q.Count > 0)
            {
                int v = q.Dequeue();
                foreach (var w in G.Adj(v))
                {
                    if (!marked[w])
                    {
                        marked[w] = true;
                        edgeTo[w] = v;
                        distTo[w] = distTo[v] + 1;
                        q.Enqueue(w);
                    }
                }
            }
        }

        public override bool HasPathTo(int v)
        {
            return marked[v];
        }

        public override IEnumerable<int> PathTo(int v)
        {
            if (!HasPathTo(v)) return null;

            Stack<int> path = new Stack<int>();
            for (int i = v; i != s; i = edgeTo[i])
            {
                path.Push(i);
            }
            path.Push(s);
            return path;
        }

    }

    public class DigraphDepthFirstPaths : Paths
    {
        private bool[] marked; // marked[v] == true, if v connected to s
        private int[] edgeTo; // edgeTo[v] == previous vertex on path from s to v
        private int s;

        public DigraphDepthFirstPaths(Digraph G, int s) : base(G, s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            edgeTo = new int[gCount];
            this.s = s;
            Dfs(G, s);
        }

        public DigraphDepthFirstPaths(Digraph G, IEnumerable<int> s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            edgeTo = new int[gCount];
            foreach (var v in s)
                if (!marked[v])
                {
                    Dfs(G, v);
                }
        }


        private void Dfs(Digraph G, int v)
        {
            marked[v] = true;
            foreach (var d in G.Adj(v))
            {
                if (!marked[d])
                {
                    Dfs(G, d);
                    edgeTo[d] = v;
                }
            }
        }

        public override bool HasPathTo(int v)
        {
            return marked[v];
        }

        public override IEnumerable<int> PathTo(int v)
        {
            if (!HasPathTo(v)) return null;

            Stack<int> path = new Stack<int>();
            for (int i = v; i != s; i = edgeTo[i])
            {
                path.Push(i);
            }
            path.Push(s);
            return path;
        }
    }


    public class DirectedDFS : Paths // finding out is there a path
    {
        private bool[] marked; // marked[v] == true, if v connected to s

        public DirectedDFS(Digraph G, int s) : base(G, s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            Dfs(G, s);
        }

        public DirectedDFS(Digraph G, IEnumerable<int> s)
        {
            int gCount = G.V();
            marked = new bool[gCount];
            foreach (var v in s)
                if (!marked[v])
                {
                    Dfs(G, v);
                }
        }


        private void Dfs(Digraph G, int v)
        {
            marked[v] = true;
            foreach (var d in G.Adj(v))
            {
                if (!marked[d])
                {
                    Dfs(G, d);
                }
            }
        }

        public override bool HasPathTo(int v)
        {
            return marked[v];
        }

    }

    public class DepthFirstOrder
    {
        private bool[] marked;
        private Queue<int> pre;  // вершины в прямом порядке
        private Queue<int> post; // вершины в обратном порядке
        private Stack<int> reversePost;  // вершины в реверсном порядке

        public DepthFirstOrder(Digraph G)
        {
            marked = new bool[G.V()];
            pre = new Queue<int>();
            post = new Queue<int>();
            reversePost = new Stack<int>();

            for (int v = 0; v < G.V(); v++)
            {
                if (!marked[v])
                {
                    Dfs(G, v);
                }
            }
        }

        private void Dfs(Digraph G, int v)
        {
            pre.Enqueue(v);
            marked[v] = true;
            foreach (var w in G.Adj(v))
            {
                if (!marked[w])
                {
                    Dfs(G, w);
                }
            }
            post.Enqueue(v);
            reversePost.Push(v);
        }

        public IEnumerable<int> Pre { get { return pre; } }
        public IEnumerable<int> Post { get { return post; } }
        public IEnumerable<int> ReversePost { get { return reversePost; } }
    }
}
