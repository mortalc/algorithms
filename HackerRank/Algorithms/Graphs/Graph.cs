﻿using System.Collections.Generic;
using System.IO;
using Utilities.Algorithms.Helpers;

namespace Utilities.Algorithms.Graphs
{
    public class Graph
    {
        protected int v;
        protected int e;
        protected Bag<int>[] adj;

        public Graph()
        { }

        public Graph(int vertices)
        {
            this.v = vertices;
            e = 0;
            adj = new Bag<int>[v];
            for (int i = 0; i < v; i++)
                adj[i] = new Bag<int>();
        }

        public Graph(StreamReader input)
        {
            this.v = int.Parse(input.ReadLine());
            int edges = int.Parse(input.ReadLine());
            for (; edges > 0; edges--)
            {
                string[] vert = input.ReadLine().Split(' ');
                int v1 = int.Parse(vert[0]);
                int v2 = int.Parse(vert[1]);
                AddEdge(v1, v2);
            }
        }

        public virtual void AddEdge(int vertex1, int vertex2)
        {
            adj[vertex1].Add(vertex2);
            adj[vertex2].Add(vertex1);
            e++;
        }

        public virtual void RemoveEdge(int vertex1, int vertex2)
        {
            adj[vertex1].Remove(vertex2);
            adj[vertex2].Remove(vertex1);
            e--;
        }

        public IEnumerable<int> Adj(int vertex)
        {
            return adj[vertex];
        }

        public int V() // VerteciesCount()
        {
            return v;
        }

        public int E() //EdgesCount
        {
            return e;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public static int Degree(Graph G, int v)
        {
            int degree = 0;
            foreach (int w in G.Adj(v)) degree++;
            return degree;
        }

        public static int MaxDegree(Graph G)
        {
            int max = 0;
            for (int v = 0; v < G.V(); v++)
                if (Degree(G, v) > max)
                    max = Degree(G, v);
            return max;
        }

        public static double AverageDegree(Graph G)
        {
            return 2.0 * G.E() / G.V();
        }

        public static int NumberOfSelfLoops(Graph G)
        {
            int count = 0;
            for (int v = 0; v < G.V(); v++)
                foreach (int w in G.Adj(v))
                    if (v == w) count++;
            return count / 2; // each edge counted twice
        }
    }
}
