﻿using System.Collections.Generic;

namespace Utilities.Algorithms.Graphs
{
    public class Topological
    {
        // Reverse DFS postorder of a DAG is a topological order
        private IEnumerable<int> order;

        public Topological(Digraph G)
        {
            DirectedCycle cycleFinder = new DirectedCycle(G);
            if (!cycleFinder.HasCycle)
            {
                DepthFirstOrder dfs = new DepthFirstOrder(G);
                order = dfs.ReversePost;
            }
        }

        public bool IsDAG() // is graph acyclic
        {
            return order == null;
        }

        public IEnumerable<int> Order() // vertices in topological order
        {
            return order;
        }
    }
}
