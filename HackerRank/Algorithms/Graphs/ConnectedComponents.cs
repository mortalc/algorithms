﻿namespace Utilities.Algorithms.Graphs
{
    public class ConnectedComponents
    {
        private bool[] marked; // marked[v] == true, if v connected to s
        private int[] id; // id[v] == id of component containing v
        private int count; // number of components 

        public ConnectedComponents(Graph G) // find Connected components in G
        {
            int gCount = G.V();
            marked = new bool[gCount];
            id = new int[gCount];
            count = 0;
            for (int v = 0; v < gCount; v++)
            {
                if (!marked[v])
                {
                    Dfs(G, v);
                    count++;
                }
            }
        }

        public bool Connected(int v, int w) // are v and w connected
        {
            return id[v] == id[w];
        }

        public int Count() // number of connected components
        {
            return count;
        }

        public int Id(int v) // component identifier for v
        {
            return id[v];
        }

        private void Dfs(Graph G, int v)
        {
            marked[v] = true;
            id[v] = count;
            foreach (var s in G.Adj(v))
            {
                if (!marked[s])
                    Dfs(G, s);
            }
        }
    }
}
