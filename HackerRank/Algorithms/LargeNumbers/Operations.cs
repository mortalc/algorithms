﻿using System;
using Utilities.Common;

namespace Utilities.Algorithms.LargeNumbers
{
    public class Operations
    {
        public static string Multiply(string aStr, string bStr)
        {
            string result = string.Empty;
            int length = aStr.Length + bStr.Length + 1;
            int[] a = aStr.SplitToIntArray();
            int[] b = bStr.SplitToIntArray();
            int[] c = new int[length];

            Array.Reverse(a);
            Array.Reverse(b);

            for (int ix = 0; ix < a.Length; ix++)
                for (int jx = 0; jx < b.Length; jx++)
                    c[ix + jx] += a[ix] * b[jx];

            for (int ix = 0; ix < length - 1; ix++)
            {
                c[ix + 1] += c[ix] / 10;
                c[ix] %= 10;
            }

            while (c[length - 1] == 0)
                length--;
            Array.Reverse(c, 0, length);
            result = string.Join("", c);
            result = result.Substring(0, length);
            return result;
        }

        public static string Addition(string aStr, string bStr)
        {
            string result = string.Empty;
            int length = 0;
            int[] a = aStr.SplitToIntArray();
            int[] b = bStr.SplitToIntArray();

            Array.Reverse(a);
            Array.Reverse(b);

            // определяем длину массива суммы
            if (a.Length > b.Length)
                length = a.Length + 1;
            else
                length = b.Length + 1;

            int[] c = new int[length];

            for (int ix = 0; ix < length - 1; ix++)
            {
                c[ix] += (ix < b.Length ? b[ix] : 0)
                    + (ix < a.Length ? a[ix] : 0); // суммируем последние разряды чисел
                c[ix + 1] += (c[ix] / 10); // если есть разряд для переноса, переносим его в следующий разряд
                c[ix] %= 10; // если есть разряд для переноса он отсекается
            }

            if (c[length - 1] == 0)
                length--;
            Array.Reverse(c, 0, length);
            result = string.Join("", c);
            result = result.Substring(0, length);
            return result;

        }

    }
}
