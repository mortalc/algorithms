﻿using System;

namespace Utilities.Algorithms.Sorting
{
    public class QuickSort
    {
        public static void Sort(IComparable[] a)
        {
            SortHelpers.Shuffle<IComparable>(a);
            //Sort(a, 0, a.Length - 1);
            Sort3Way(a, 0, a.Length - 1);
        }

        private static void Sort(IComparable[] a, int lo, int hi)
        {
            //if (lo >= hi)
            //    return;
            // улучшение
            if (lo + SortHelpers.INS_SORT_BOARD >= hi)
            {
                InsertionSort.Sort(a, lo, hi);
                return;
            }

            int k = Partition(a, lo, hi);
            Sort(a, lo, k - 1);
            Sort(a, k + 1, hi);
        }

        private static int Partition(IComparable[] a, int lo, int hi)
        {
            IComparable item = a[lo];
            int l = lo + 1, k = hi;
            while (l < k)
            {
                while (SortHelpers.Less(a[l], item))
                {
                    if (++l >= hi)
                        break;
                }

                while (SortHelpers.Less(item, a[k]))
                {
                    if (--k <= lo)
                        break;
                }
                if (l >= k)
                    break;
                SortHelpers.Exch(a, l, k);
            }
            SortHelpers.Exch(a, lo, k);
            return k;
        }

        public static void Sort3Way(IComparable[] a)
        {
            SortHelpers.Shuffle<IComparable>(a);
            Sort3Way(a, 0, a.Length - 1);
        }

        private static void Sort3Way(IComparable[] a, int lo, int hi)
        {
            if (lo + SortHelpers.INS_SORT_BOARD >= hi)
            {
                InsertionSort.Sort(a, lo, hi);
                return;
            }

            //if (lo >= hi)
            //    return;

            int i = lo + 1, lt = lo, gt = hi;
            IComparable val = a[lo];
            while (i <= gt)
            {
                if (a[i].CompareTo(val) < 0)
                {
                    SortHelpers.Exch(a, i, lt);
                    i++;
                    lt++;
                }
                else if (a[i].CompareTo(val) > 0)
                {
                    SortHelpers.Exch(a, i, gt);
                    gt--;
                }
                else i++;
            }

            Sort3Way(a, lo, lt - 1);
            Sort3Way(a, gt + 1, hi);
        }

    }
}
