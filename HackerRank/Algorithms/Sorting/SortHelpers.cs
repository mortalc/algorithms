﻿using System;
using System.Linq;

namespace Utilities.Algorithms.Sorting
{
    public static class SortHelpers
    {
        public const int INS_SORT_BOARD = 5;

        public static bool Less(IComparable a, IComparable b)
        {
            return a.CompareTo(b) < 0;
        }

        public static void Exch(IComparable[] a, int i, int j)
        {
            IComparable temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }

        public static IComparable[] ToComparable(int[] a)
        {
            IComparable[] b = new IComparable[a.Length];
            for (int i = 0; i < a.Length; i++)
                b[i] = a[i];
            return b;
        }

        public static int[] ToInt(IComparable[] a)
        {
            int[] b = new int[a.Length];
            for (int i = 0; i < a.Length; i++)
                b[i] = (int)a[i];
            return b;
        }

        public static void Shuffle<T>(T[] a)
        {
            Random rnd = new Random();
            a = a.OrderBy(x => rnd.Next()).ToArray();
        }
    }
}
