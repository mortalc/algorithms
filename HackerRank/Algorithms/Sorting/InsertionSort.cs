﻿using System;

namespace Utilities.Algorithms.Sorting
{
    public class InsertionSort
    {
        public static void Sort(IComparable[] a)
        {
            //for (int i = 1; i < a.Length; i++)
            //    for (int j = i; j > 0; j--)
            //    {
            //        if (SortHelpers.Less(a[j], a[j-1]))
            //            SortHelpers.Exch(a, j, j - 1);
            //    }

            Sort(a, 0, a.Length - 1);
        }

        public static void Sort(IComparable[] a, int lo, int hi)
        {
            for (int i = lo + 1; i < hi + 1; i++)
                for (int j = i; j > lo; j--)
                {
                    if (SortHelpers.Less(a[j], a[j - 1]))
                        SortHelpers.Exch(a, j, j - 1);
                }
        }

    }
}
