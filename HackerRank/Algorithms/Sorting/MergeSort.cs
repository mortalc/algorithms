﻿using System;

namespace Utilities.Algorithms.Sorting
{
    public class MergeSort
    {
        //IComparable
        //Int32
        private static IComparable[] aux;

        private static void Merge(IComparable[] a, int lo, int mid, int high)
        {
            // Слияние a[lo..mid] a[mid+1..high]
            int i = lo, j = mid + 1;

            for (int k = lo; k <= high; k++)
                aux[k] = a[k];
            for (int k = lo; k <= high; k++)
            {
                if (i > mid)
                    a[k] = aux[j++];
                else if (j > high)
                    a[k] = aux[i++];
                else if (Less(aux[j], aux[i]))
                    a[k] = aux[j++];
                else
                    a[k] = aux[i++];
            }
        }

        public static void Sort(IComparable[] a)
        {
            aux = new IComparable[a.Length];
            Sort(a, 0, a.Length - 1);
        }

        private static void Sort(IComparable[] a, int lo, int high)
        {
            // сортировка a[lo..high]
            //if (lo >= high)
            //    return;
            // улучшение
            if (lo + SortHelpers.INS_SORT_BOARD >= high)
            {
                InsertionSort.Sort(a, lo, high);
                return;
            }
            int mid = (high + lo) / 2;
            Sort(a, lo, mid);
            Sort(a, mid + 1, high);
            //if (Less(a[mid], a[mid + 1])) // улучшение, слияние не нужно
            //    return;
            Merge(a, lo, mid, high);
        }

        private static bool Less(IComparable a, IComparable b)
        {
            return a.CompareTo(b) < 0;
        }
    }


    public class MergeSortBU
    {
        //IComparable
        //Int32
        private static IComparable[] aux;

        private static void Merge(IComparable[] a, int lo, int mid, int high)
        {
            // Слияние a[lo..mid] a[mid+1..high]
            int i = lo, j = mid + 1;

            for (int k = lo; k <= high; k++)
                aux[k] = a[k];
            for (int k = lo; k <= high; k++)
            {
                if (i > mid)
                    a[k] = aux[j++];
                else if (j > high)
                    a[k] = aux[i++];
                else if (Less(aux[j], aux[i]))
                    a[k] = aux[j++];
                else
                    a[k] = aux[i++];
            }
        }

        public static void Sort(IComparable[] a)
        {
            aux = new IComparable[a.Length];
            for (int sz = 1; sz < a.Length; sz = sz + sz)
                for (int lo = 0; lo < a.Length - sz; lo += sz + sz)
                    Merge(a, lo, lo + sz - 1, Math.Min(lo + sz + sz - 1, a.Length - 1));
        }

        private static bool Less(IComparable a, IComparable b)
        {
            return a.CompareTo(b) < 0;
        }
    }
}
