﻿using System;

namespace Utilities.Algorithms.Sorting
{
    public class PyramidSort
    {
        public static void Sort(IComparable[] a)
        {
            int N = a.Length;

            for (int i = N / 2; i >= 1; i--)
            {
                Sink(a, i, N);
            }

            while (N > 1)
            {
                Exch(a, 1, N--);
                Sink(a, 1, N);
            }
        }

        private static void Sink(IComparable[] a, int k, int N)
        {
            while (2 * k <= N)
            {
                int j = k * 2;
                if (j < N && Less(a, j, j + 1)) j++;
                if (!Less(a, k, j)) break;
                Exch(a, j, k);
                k = j;
            }
        }

        protected static bool Less(IComparable[] a, int i, int j)
        {
            i--;
            j--;
            return a[i].CompareTo(a[j]) < 0;
        }

        protected static void Exch(IComparable[] a, int i, int j)
        {
            i--;
            j--;
            IComparable t = a[i];
            a[i] = a[j];
            a[j] = t;
        }
    }
}
