﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Problems.Algorithms.Implementation
{
    public class FindNextString
    {
        static string FindAlphabeticallyNextString(string s)
        {
            for (int j = s.Length - 1; j > 0; j--)
            {
                if (s[j] > s[j - 1])
                {
                    // search for lowest char
                    int nearestPos = j;
                    for (int v = j; v < s.Length; v++)
                    {
                        if (s[v] > s[j - 1] && s[v] < s[j])
                        {
                            nearestPos = v;
                        }
                    }
                    // replace them
                    char[] cS = s.ToCharArray();
                    char c = cS[j - 1];
                    cS[j - 1] = cS[nearestPos];
                    cS[nearestPos] = c;

                    // sort ending part
                    char[] end = new char[s.Length - j];
                    for (int f = j; f < s.Length; f++)
                    {
                        end[f - j] = cS[f];
                    }
                    Array.Sort(end);

                    // copy back
                    for (int f = j; f < s.Length; f++)
                    {
                        cS[f] = end[f - j];
                    }

                    return new string(cS);
                }
            }
            return string.Empty;
        }
    }
}
/*
 Given a word , rearrange the letters of  to construct another word  in such a way that  is lexicographically greater than . In case of multiple possible answers, find the lexicographically smallest one among them.

Input Format

The first line of input contains , the number of test cases. Each of the next  lines contains .

Constraints

 will contain only lower-case English letters and its length will not exceed .
Output Format

For each testcase, output a string lexicographically bigger than  in a separate line. In case of multiple possible answers, print the lexicographically smallest one, and if no answer exists, print no answer.

Sample Input

5
ab
bb
hefg
dhck
dkhc
Sample Output

ba
no answer
hegf
dhkc
hcdk
Explanation

Test case 1: 
There exists only one string greater than ab which can be built by rearranging ab. That is ba.
Test case 2: 
Not possible to rearrange bb and get a lexicographically greater string.
Test case 3: 
hegf is the next string lexicographically greater than hefg.
Test case 4: 
dhkc is the next string lexicographically greater than dhck.
Test case 5: 
hcdk is the next string lexicographically greater than dkhc.
 */
