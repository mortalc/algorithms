﻿using System;

namespace Utilities.Problems
{
    public class QueensRevised
    {
        public string Solution(int N)
        {
            N = Math.Min(N, 999);
            string result = "";
            for (int i = 1; i <= N; i += 2)
            {
                result += string.Format("{0} ", i);
            }
            for (int i = 2; i <= N; i += 2)
            {
                result += string.Format("{0} ", i);
            }
            return result;
        }

        bool align3(int x1, int y1, int x2, int y2, int x, int y)
        {
            return (x1 - x) * (y2 - y) == (x2 - x) * (y1 - y);
        }
    }
}
