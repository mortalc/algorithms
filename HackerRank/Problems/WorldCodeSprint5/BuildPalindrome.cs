﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Problems.WorldCodeSprint5
{
    public class BuildPalindrome
    {
        public static string Solution(string sA, string sB)
        {
            int j = 1;
            int maxLength = 0;
            List<string> list = new List<string>();

            for (int i = 0; i <= sA.Length - j && j <= sA.Length; i++)
            {
                string substrA = sA.Substring(i, j);
                string pattern = Reverse(substrA);
                int start = 0;
                start = sB.IndexOf(pattern, start);
                while (start >= 0)
                {
                    int palindromeLength = sA.Length - i - j;
                    string substrAExtended = "";
                    while (palindromeLength > 0)
                    {
                        // search for palindrome symbols in sA after i+j
                        if (IsPalindrome(sA.Substring(i + j, palindromeLength)))
                            break;
                        palindromeLength--;
                    }
                    substrAExtended = sA.Substring(i + j, palindromeLength);

                    if (maxLength < substrAExtended.Length + pattern.Length * 2)
                    {
                        list.Clear();
                        maxLength = substrAExtended.Length + pattern.Length * 2;
                        list.Add(substrA + substrAExtended + pattern);
                    }
                    else if (substrAExtended.Length + pattern.Length * 2 == maxLength)
                        list.Add(substrA + substrAExtended + pattern);

                    int palindromeIndex = 0;
                    string patternExtended = "";
                    while (palindromeIndex <= start - 1)
                    {
                        // search for palindrome in sB before start
                        if (IsPalindrome(sB.Substring(palindromeIndex, start - palindromeIndex)))
                            break;
                        palindromeIndex++;
                    }
                    patternExtended = sB.Substring(palindromeIndex, start - palindromeIndex);
                    if (maxLength < substrA.Length * 2 + patternExtended.Length)
                    {
                        list.Clear();
                        maxLength = substrA.Length * 2 + patternExtended.Length;
                        list.Add(substrA + patternExtended + pattern);
                    }
                    else if (substrA.Length * 2 + patternExtended.Length == maxLength)
                        list.Add(substrA + patternExtended + pattern);

                    if (maxLength < substrA.Length + pattern.Length)
                    {
                        list.Clear();
                        maxLength = substrA.Length * 2;
                        list.Add(substrA + pattern);
                    }

                    start++;
                    if (start >= sB.Length)
                        break;
                    start = sB.IndexOf(pattern, start);
                }

                if (i == sA.Length - j)
                {
                    j++;
                    i = -1;
                }
            }

            list.Sort();

            if (list.Count == 0 || list[0].Length < 2)
                return "-1";
            else
                return list[0];
            
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        static bool IsPalindrome(string s)
        {
            if (string.IsNullOrEmpty(s))
                return false;
            if (s.Length == 1)
                return true;
            for (int i = 0; i < s.Length / 2; i++)
                if (s[i] != s[s.Length - 1 - i])
                    return false;
            return true;
        }
    }
}


/*
You have two strings,  and . Find a string, , such that:

 can be expressed as  where  is a non-empty substring of  and  is a non-empty substring of .
 is a palindromic string.
The length of  is as long as possible.
For each of the  pairs of strings ( and ) received as input, find and print string  on a new line. If you're able to form more than one valid string , print whichever one comes first alphabetically. If there is no valid answer, print  instead.

Input Format

The first line contains a single integer, , denoting the number of queries. The subsequent lines describe each query over two lines:

The first line contains a single string denoting .
The second line contains a single string denoting .
Constraints

 and  contain only lowercase English letters.
Sum of |a| over all queries does not exceed 
Sum of |b| over all queries does not exceed 
Output Format

For each pair of strings ( and ), find some  satisfying the conditions above and print it on a new line. If there is no such string, print  instead.

Sample Input

3
bac
bac
abc
def
jdfh
fds
Sample Output

aba
-1
dfhfd
Explanation

We perform the following three queries:

Concatenate  with  to create .
We're given  and ; because both strings are composed of unique characters, we cannot use them to form a palindromic string. Thus, we print .
Concatenate  with  to create . Note that we chose these particular substrings because the length of string  must be maximal. 
 
 */