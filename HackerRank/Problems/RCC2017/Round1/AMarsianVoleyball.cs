﻿using System;

namespace HackerRank.Problems.RCC2017.Round1
{
    public class AMarsianVoleyball
    {
        public static void Main(string[] args)
        {

            int numOfTests = int.Parse(Console.ReadLine());

            for (int i = 0; i < numOfTests; i++)
            {
                int[] arr = AMarsianVoleyball.ReadArr<int>(Int32.Parse);
                int solution = Solve(arr[0], arr[1], arr[2]);
                Console.WriteLine(solution);
            }
        }

        public static int Solve(int k, int x, int y)
        {
            int maxSteps = 2;
            int difference = Math.Abs(x - y);
            int max = Math.Max(x, y);
            if (max >= k - 1)
            {
                if (difference < maxSteps)
                {
                    return maxSteps - difference;
                }
                return 1;
            }
            else
            {
                return k - max;
            }
        }

        public static T[] ReadArr<T>(Converter<string, T> converter) where T : struct
        {
            string[] arr_temp = Console.ReadLine().Split(' ');
            T[] arr = Array.ConvertAll(arr_temp, converter);
            return arr;
        }
    }
}
