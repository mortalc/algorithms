﻿using System;
using System.Linq;

namespace HackerRank.Problems.RCC2017.WarmUp
{
    class ASpaceship
    {
        public static void Main(string[] args)
        {
            int arrLength = int.Parse(Console.ReadLine());
            int[] arr = ASpaceship.ReadArr<int>(Int32.Parse);

            int sum = arr.Sum();
            int number = 0;
            int index = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == (sum - arr[i]))
                {
                    number = arr[i];
                    index = i;
                    break;
                }
            }

            arr = arr.Where((val, idx) => idx != index).ToArray<int>();
            string arrSplit = string.Join(" ", arr.Select(x => x.ToString()).ToArray());
            Console.WriteLine(arrSplit + ' ' + number);
        }

        public static T[] ReadArr<T>(Converter<string, T> converter) where T : struct
        {
            string[] arr_temp = Console.ReadLine().Split(' ');
            T[] arr = Array.ConvertAll(arr_temp, converter);
            return arr;
        }
    }
}
