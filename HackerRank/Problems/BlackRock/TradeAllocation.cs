﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Problems.BlackRock
{
    public class TradeAllocation
    {
        //int MinTradeSize = settings[0];
        //int Increment = settings[1];
        //int AvailableUnits = settings[2];

        private static int MinTradeSize;
        private static int Increment;
        private static int AvailableUnits;
        private static int AvailableUnitsStart;

        private static bool IsAmountTradeable(int amount)
        {
            if (amount == 0)
            {
                return true;
            }
            return (amount - MinTradeSize) % Increment == 0;
        }

        private static bool IsLeftAmountTradeable(int amount)
        {
            if (amount == 0)
            {
                return true;
            }
            return (amount) % Increment == 0;
        }

        private static int GetClosestAmountTradeable(double amount)
        {
            if (amount <= MinTradeSize)
            {
                return 0;
            }
            int closest = (int)Math.Floor((double)((double)(amount - MinTradeSize) / (double)Increment));
            closest = closest * Increment + MinTradeSize;
            return closest;
        }

        public static List<Order> Solution(int minTradeSize, int increment, int avalilableUnits,
                                    List<Order> orders)
        {
            MinTradeSize = minTradeSize;
            Increment = increment;
            AvailableUnitsStart = AvailableUnits = avalilableUnits;

            // sort
            orders = orders.OrderBy(x => x.AllocatedAmount).ThenBy(x => x.Portfolio).ToList<Order>();
            if (avalilableUnits < MinTradeSize)
                return orders;
            // processing
            int totalOrder = orders.Sum(x => x.OrderAmount);
            bool shouldCount = false;
            if (totalOrder < avalilableUnits)
            {
                foreach (var item in orders)
                {
                    if (!IsAmountTradeable(item.OrderAmount))
                    {
                        shouldCount = true;
                        break;
                    }
                }
            }
            else
            {
                shouldCount = true;
            }
            if (shouldCount)
            {
                orders = Process(orders);
            }

            return orders;
        }

        public static List<Order> Process(List<Order> orders)
        {
            int totalOrder = orders.Sum(x => x.OrderAmount - x.AllocatedAmount);

            double allocatedAmount = 0.0;
            bool canAllocate = false;
            if (AvailableUnits > 0 && totalOrder > 0)
            {
                foreach (var item in orders)
                {
                    if (AvailableUnits == 0)
                        break;
                    if (item.OrderAmount == 0 && item.OrderAmount < MinTradeSize)
                    {
                        continue;
                    }
                    allocatedAmount = ((double)(item.OrderAmount - item.AllocatedAmount) / (double)totalOrder) * AvailableUnits;
                    if (allocatedAmount < MinTradeSize)
                    {
                        if (allocatedAmount >= MinTradeSize / 2)
                        {
                            canAllocate = true;
                            if (item.AllocatedAmount < item.OrderAmount)
                            {
                                if (AvailableUnits - MinTradeSize < 0)
                                {
                                    if (item.AllocatedAmount > 0)
                                    {
                                        item.AllocatedAmount += AvailableUnits;
                                        AvailableUnits = 0;
                                    }
                                }

                                else
                                {
                                    item.AllocatedAmount += MinTradeSize;

                                }
                            }
                        }
                    }
                    else
                    {
                        if (allocatedAmount >= item.OrderAmount - item.AllocatedAmount)
                        {
                            item.AllocatedAmount += (item.OrderAmount - item.AllocatedAmount);
                            //continue;
                        }
                        if (!IsAmountTradeable((int)(allocatedAmount)))
                        {
                            int alloc = GetClosestAmountTradeable(allocatedAmount);
                            if (IsAmountTradeable(item.OrderAmount - (item.AllocatedAmount + alloc)))
                            {
                                item.AllocatedAmount += alloc;
                                canAllocate = true;
                            }

                        }
                        else
                        {
                            if (IsAmountTradeable(item.OrderAmount - (item.AllocatedAmount + (int)allocatedAmount)))
                            {
                                item.AllocatedAmount += (int)(allocatedAmount);
                                canAllocate = true;
                            }
                        }

                    }

                    if (item.AllocatedAmount > item.OrderAmount)
                        item.AllocatedAmount = item.OrderAmount;
                    if (IsAmountTradeable(item.OrderAmount - GetClosestAmountTradeable(item.AllocatedAmount)))
                        item.AllocatedAmount = GetClosestAmountTradeable(item.AllocatedAmount);
                }
            }

            AvailableUnits = AvailableUnitsStart - orders.Sum(x => x.AllocatedAmount);
            if (AvailableUnits > 0 && canAllocate)
                orders = Process(orders);
            return orders;

        }


    }

    public class Order
    {
        public string Portfolio { get; set; }
        public int OrderAmount { get; set; }
        public int AllocatedAmount { get; set; }

    }
}
