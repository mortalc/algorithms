﻿using Utilities.Problems;
using NUnit.Framework;

namespace hackerRankTest.Problems
{
    [TestFixture]
    public class QueensRevisedTest
    {
        [Test]
        [TestCase(11, "2 4 7 1 8 11 5 3 9 6 10")]
        public void CheckSolution(int N, string exp)
        {
            QueensRevised problem = new QueensRevised();
            string act = problem.Solution(N);
            Assert.AreEqual(exp, act);
        }
    }
}
