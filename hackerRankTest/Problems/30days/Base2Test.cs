﻿using System;
using HackerRank.Common;
using NUnit.Framework;

namespace hackerRankTest.Problems
{
    [TestFixture]
    public class Base2Test
    {
        [Test]
        [TestCase(new int[] { 1, 0, 1 }, 5, 2)]
        [TestCase(new int[] { 1, 1, 0, 0, 1 }, 9, -2)]
        [TestCase(new int[] { 1, 1, 0, 1, 0, 1, 1 }, 23, -2)]
        [TestCase(new int[] { 1, 1, 1, 0, 0, 1 }, -23, -2)]
        public void CheckBaseRepresentation(int[] exp, int number, int toBase)
        {
            int[] act = ConvertToBase.GetBaseRepresentation(number, toBase);
            Assert.AreEqual(string.Concat(exp), string.Concat(act));
        }
    }
}