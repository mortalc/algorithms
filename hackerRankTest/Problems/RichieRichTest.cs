﻿using Utilities.Problems;
using NUnit.Framework;

namespace hackerRankTest.Problems
{
    [TestFixture]
    public class RichieRichTest
    {
        [Test]
        [TestCase("3993", 4, 1, "3943")]
        [TestCase("992299", 6, 3, "092282")]
        public void CheckRichieRich(string exp, int digits, int changes, string number)
        {
            RichieRich problem = new RichieRich();
            string act = problem.Solution(digits, changes, number);
            Assert.AreEqual(exp, act);
        }
    }
}
