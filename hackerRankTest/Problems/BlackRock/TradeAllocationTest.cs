﻿using HackerRank.Problems.BlackRock;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace hackerRankTest.Problems.BlackRock
{

    [TestFixture]
    class TradeAllocationTest
    {
        [Test]
        [TestCase(10, 2, 40)]
        public void TradeAllocShouldPass(int minTradeSize, int increment, int avalilableUnits)
        {
            var orders = new List<Order> { new Order { OrderAmount = 16, Portfolio = "p1" },
                                  new Order { OrderAmount = 134, Portfolio = "p2" }};
            var exp = new List<Order> { new Order { Portfolio = "p1", AllocatedAmount = 0 },
                                  new Order { Portfolio = "p2", AllocatedAmount = 40 } };


            List<Order> act = TradeAllocation.Solution(minTradeSize, increment, avalilableUnits, orders);
            foreach (var item in act)
            {
                var expItem = exp.Where(x => x.Portfolio == item.Portfolio).First();
                Assert.AreEqual(expItem.AllocatedAmount, item.AllocatedAmount);
            }
        }

        [Test]
        [TestCase(14, 5, 999)]
        public void TradeAllocShouldPass2(int minTradeSize, int increment, int avalilableUnits)
        {
            var orders = new List<Order> {
                new Order { OrderAmount = 364, Portfolio = "p1" },
                new Order { OrderAmount = 179, Portfolio = "p2" },
                new Order { OrderAmount = 354, Portfolio = "p3" },
                new Order { OrderAmount = 334, Portfolio = "p4" },
                new Order { OrderAmount = 119, Portfolio = "p5" }
            };
            var exp = new List<Order> {
                new Order { AllocatedAmount = 364, Portfolio = "p1" },
                new Order { AllocatedAmount = 0, Portfolio = "p2" },
                new Order { AllocatedAmount = 354, Portfolio = "p3" },
                new Order { AllocatedAmount = 0, Portfolio = "p4" },
                new Order { AllocatedAmount = 0, Portfolio = "p5" }
                };


            List<Order> act = TradeAllocation.Solution(minTradeSize, increment, avalilableUnits, orders);
            foreach (var item in act)
            {
                var expItem = exp.Where(x => x.Portfolio == item.Portfolio).First();
                Assert.AreEqual(expItem.AllocatedAmount, item.AllocatedAmount);
            }
        }
    }
}
