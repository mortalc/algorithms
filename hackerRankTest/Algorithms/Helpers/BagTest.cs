﻿using Utilities.Algorithms.Helpers;
using NUnit.Framework;


namespace hackerRankTest.Algorithms.Helpers
{
    [TestFixture]
    class BagTest
    {
        [Test]
        [TestCase(" b v a")]
        public void BagShouldPass(string exp)
        {
            string act = string.Empty;
            Bag<string> bag = new Bag<string>();
            bag.Add("a");
            bag.Add("v");
            bag.Add("b");

            foreach (var val in bag)
                act = string.Concat(act, " ", val);

            Assert.AreEqual(exp, act);
        }

    }
}
