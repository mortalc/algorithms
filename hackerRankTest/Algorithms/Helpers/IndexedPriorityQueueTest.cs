﻿using NUnit.Framework;
using System.Diagnostics;
using Utilities.Algorithms.Helpers;

namespace hackerRankTest.Algorithms.Helpers
{
    [TestFixture]
    class IndexedPriorityQueueTest
    {
        [Test]
        [TestCase("2 3 1 ")]
        public void IndexedPriorityQueueMaxShouldPass(string exp)
        {
            IndexedMaxPQ<int> q = new IndexedMaxPQ<int>(4);
            string act = string.Empty;

            q.Insert(2, 30);
            q.Insert(4, 40);
            q.Insert(1, 10);
            q.Insert(3, 20);
            q.DelMax();

            while (!q.IsEmpty())
                act += (q.DelMax() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("4 2 3 ")]
        public void IndexedPriorityQueueMaxIncreaseKeyShouldPass(string exp)
        {
            IndexedMaxPQ<int> q = new IndexedMaxPQ<int>(4);
            string act = string.Empty;

            q.Insert(2, 30);
            q.Insert(4, 40);
            q.Insert(1, 10);
            q.Insert(3, 20);
            q.IncreaseKey(1, 50);
            q.DelMax();

            while (!q.IsEmpty())
                act += (q.DelMax() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("best it it of the the times was was worst ")]
        public void IndexedPriorityQueueMinShouldPassString(string exp)
        {
            string[] strings = { "it", "was", "the", "best", "of", "times", "it", "was", "the", "worst" };

            IndexedMinPQ<string> q = new IndexedMinPQ<string>(10);
            string act = string.Empty;

            for (int i = 0; i < strings.Length; i++)
            {
                q.Insert(i, strings[i]);
            }


            while (!q.IsEmpty())
            {
                int i = q.DelMin();
                act += (strings[i] + " ");
                Debug.WriteLine(string.Format("i {0} = {1}", i, strings[i]));
            }
            Assert.AreEqual(exp, act);
        }

    }
}
