﻿using Utilities.Algorithms.Helpers;
using NUnit.Framework;

namespace hackerRankTest.Algorithms.Helpers
{
    [TestFixture]
    class QueueTest
    {
        [Test]
        [TestCase("3 2 1 ")]
        public void QueueArrShouldPass(string exp)
        {
            Queue<string> q = new Queue<string>();
            string act = string.Empty;
            q.Enqueue("5");
            q.Enqueue("3");
            q.Dequeue();
            q.Enqueue("2");
            q.Enqueue("1");
            while (!q.IsEmpty())
                act += (q.Dequeue() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("3 2 1 ")]
        public void QueueListShouldPass(string exp)
        {
            Queue<string> q = new Queue<string>();
            string act = string.Empty;
            q.Enqueue("5");
            q.Enqueue("3");
            q.Dequeue();
            q.Enqueue("2");
            q.Enqueue("1");
            while (!q.IsEmpty())
                act += (q.Dequeue() + " ");
            Assert.AreEqual(exp, act);
        }
    }
}
