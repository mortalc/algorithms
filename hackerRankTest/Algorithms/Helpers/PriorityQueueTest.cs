﻿using NUnit.Framework;
using Utilities.Algorithms.Helpers;

namespace hackerRankTest.Algorithms.Helpers
{
    [TestFixture]
    class PriorityQueueTest
    {
        [Test]
        [TestCase("3 2 1 ")]
        public void PriorityQueueShouldPass(string exp)
        {
            MaxPQ<int> q = new MaxPQ<int>();
            string act = string.Empty;

            q.insert(2);
            q.insert(4);
            q.insert(1);
            q.insert(3);
            q.delMax();

            while (!q.isEmpty())
                act += (q.delMax() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("3 2 1 ")]
        public void PriorityQueueMaxItemsShouldPass(string exp)
        {
            MaxPQ<int> q = new MaxPQ<int>(4);
            string act = string.Empty;

            q.insert(2);
            q.insert(4);
            q.insert(1);
            q.insert(3);
            q.delMax();

            while (!q.isEmpty())
                act += (q.delMax() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("2 3 4 ")]
        public void PriorityQueueMin1ShouldPass(string exp)
        {
            MinPQ<int> q = new MinPQ<int>();
            string act = string.Empty;

            q.insert(2);
            q.insert(4);
            q.insert(1);
            q.insert(3);
            q.delMin();

            while (!q.isEmpty())
                act += (q.delMin() + " ");
            Assert.AreEqual(exp, act);
        }

    }
}
