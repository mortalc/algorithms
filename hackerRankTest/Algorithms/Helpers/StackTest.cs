﻿using Utilities.Algorithms.Helpers;
using NUnit.Framework;

namespace hackerRankTest.Algorithms.Helpers
{
    [TestFixture]
    class StackTest
    {
        [Test]
        [TestCase("1 2 5 ")]
        public void StackArrShouldPass(string exp)
        {
            StackArr<string> stack = new StackArr<string>();
            string act = string.Empty;
            stack.Push("5");
            stack.Push("1");
            stack.Pop();
            stack.Push("2");
            stack.Push("1");
            while (!stack.IsEmpty())
                act += (stack.Pop() + " ");
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("1 2 5 ")]
        public void StackListShouldPass(string exp)
        {
            StackList<string> stack = new StackList<string>();
            string act = string.Empty;
            stack.Push("5");
            stack.Push("1");
            stack.Pop();
            stack.Push("2");
            stack.Push("1");
            while (!stack.IsEmpty())
                act += (stack.Pop() + " ");
            Assert.AreEqual(exp, act);
        }
    }
}
