﻿using HackerRank.Algorithms.Strings;
using NUnit.Framework;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class LongestCommonSubsequenceTest
    {
        [Test]
        [TestCase(15, "WEWOUCUIDGCGTRMEZEPXZFEJWISRSBBSYXAYDFEJJDLEBVHHKS",
                      "FDAGCXGKCTKWNECHMRXZWMLRYUCOCZHJRRJBOAJOQJZZVUYXIC")]
        // DGCGTEXZWRYAJJV

        [TestCase(3, "SHINCHAN",
                     "NOHARAAA")] // NHA
        public void LCSubsecuenceTestShouldPass(int exp, string s1, string s2)
        {
            int act = LongestCommonSubsequence.LCSLength(s1, s2);

            Assert.AreEqual(exp, act);
        }

    }
}
