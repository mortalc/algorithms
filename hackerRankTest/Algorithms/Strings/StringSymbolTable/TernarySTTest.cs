﻿using NUnit.Framework;
using System.Collections.Generic;
using Utilities.Algorithms.Strings.StringsSymbolTable;

namespace hackerRankTest.Algorithms.Strings.StringSymbolTable
{
    [TestFixture]
    class TernarySTTest
    {
        [Test]
        public void TSTKeysTestShouldPass()
        {
            string[] s = new string[] { "dab", "add", "cab", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "aabbdd", "ace" };
            string[] exp = new string[] { "aabbdd", "ace", "add", "bad", "bed", "bee", "cab", "dab", "dad", "ebb", "fad", "fed", "fee" };
            IEnumerable<string> act;

            TernaryST<string> st = new TernaryST<string>();
            foreach (string str in s)
            {
                st.Put(str, "abc");
            }

            act = st.Keys;

            int i = 0;
            foreach (var str in act)
            {
                Assert.AreEqual(exp[i++], str);
            }
        }

        [Test]
        public void TSTKeysWithPrefixTestShouldPass()
        {
            string[] s = new string[] { "dab", "dabgt", "add", "cab", "dabre", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "aabbdd", "ace" };
            string[] exp = new string[] { "dab", "dabgt", "dabre" };
            IEnumerable<string> act;

            TernaryST<string> st = new TernaryST<string>();
            foreach (string str in s)
            {
                st.Put(str, "abc");
            }

            act = st.KeysWithPrefix("dab");

            int i = 0;
            foreach (var str in act)
            {
                Assert.AreEqual(exp[i++], str);
            }
        }

        [Test]
        public void TSTKeysThatMatchTestShouldPass()
        {
            string[] s = new string[] { "dab", "dabgt", "add", "cab", "dabre", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "aabbdd", "ace" };
            string[] exp = new string[] { "cab", "dab" };
            IEnumerable<string> act;

            TernaryST<string> st = new TernaryST<string>();
            foreach (string str in s)
            {
                st.Put(str, "abc");
            }

            act = st.KeysThatMatch(".ab");

            int i = 0;
            foreach (var str in act)
            {
                Assert.AreEqual(exp[i++], str);
            }
        }

        [Test]
        public void TSTLongestPrefixOfTestShouldPass()
        {
            string[] s = new string[] { "dab", "dabgt", "add", "cab", "dabre", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "aabbdd", "ace" };
            string exp = "dab";
            string act;

            TernaryST<string> st = new TernaryST<string>();
            foreach (string str in s)
            {
                st.Put(str, "abc");
            }

            act = st.LongestPrefixOf("dabgkl");

            Assert.AreEqual(exp, act);
        }
    }
}
