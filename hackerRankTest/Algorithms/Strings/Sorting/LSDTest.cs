﻿using NUnit.Framework;
using Utilities.Algorithms.Strings.Sorts;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class LSDTest
    {
        [Test]
        public void LSDTestShouldPass()
        {
            string[] s = new string[] { "dab", "add", "cab", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "ace" };
            string[] exp = new string[] { "ace", "add", "bad", "bed", "bee", "cab", "dab", "dad", "ebb", "fad", "fed", "fee" };

            //string[] act = LSD.Sort(s, 3);
            //for (int i = 0; i < act.Length; i++)
            //{
            //    Assert.AreEqual(exp[i], act[i]);    
            //}            

            LSD.Sort(s, 3);
            for (int i = 0; i < s.Length; i++)
            {
                Assert.AreEqual(exp[i], s[i]);
            }
        }

    }
}
