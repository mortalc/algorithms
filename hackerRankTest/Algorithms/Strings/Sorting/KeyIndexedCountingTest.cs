﻿using NUnit.Framework;
using Utilities.Algorithms.Strings.Sorts;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class KeyIndexedCountingTest
    {
        [Test]
        [TestCase("ehllo", "hello")]
        [TestCase("aehllo", "helalo")]
        [TestCase("abehllo", "helalob")]
        public void KeyIndexedCountingSortTest(string exp, string s)
        {
            string act = KeyIndexedCounting.Sort(s);
            Assert.AreEqual(exp, act);
        }

    }
}
