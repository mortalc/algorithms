﻿using NUnit.Framework;
using Utilities.Algorithms.Strings.Sorts;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class MSDTest
    {
        [Test]
        public void MSDTestShouldPass()
        {
            string[] s = new string[] { "dab", "add", "cab", "fad", "fee", "bad", "dad", "bee", "fed", "bed", "ebb", "aabbdd", "ace" };
            string[] exp = new string[] { "aabbdd", "ace", "add", "bad", "bed", "bee", "cab", "dab", "dad", "ebb", "fad", "fed", "fee" };

            MSD.Sort(s);
            for (int i = 0; i < s.Length; i++)
            {
                Assert.AreEqual(exp[i], s[i]);
            }
        }

    }
}
