﻿using NUnit.Framework;
using Utilities.Algorithms.Strings.Sorts;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class LRSTest
    {
        [Test]
        public void LRSTestShouldPass()
        {
            string s = "asdf hello weekend adn so on. this was last week  when i saw you";
            string exp = "week";

            string act = LongestRepeatedSubstring.Lrs(s);
            act = act.Trim();
            Assert.AreEqual(exp, act);
        }

    }
}
