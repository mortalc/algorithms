﻿using NUnit.Framework;
using Utilities.Algorithms.Strings.SearchSubstring;
using Utilities.Algorithms.Strings.Sorts;

namespace hackerRankTest.Algorithms.Strings.Sorting
{
    [TestFixture]
    class BruteForceTest
    {
        [Test]
        public void BruteForceTestShouldPass()
        {
            string s = "dseabcrt";
            string pattern = "abc";

            int exp = 3;

            ISearchSubstring search = new BruteForce();

            Assert.AreEqual(exp, search.Search(pattern, s));
        }

        [Test]
        public void BruteForceTestNotFoundRetMinusOne()
        {
            string s = "dseabcrt";
            string pattern = "abcd";

            int exp = SearchSubstring.NOT_FOUND;

            ISearchSubstring search = new BruteForce();

            Assert.AreEqual(exp, search.Search(pattern, s));
        }

    }
}
