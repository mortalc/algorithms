﻿using NUnit.Framework;
using Utilities.Algorithms.LargeNumbers;

namespace hackerRankTest.Algorithms.LargeNumbers
{
    [TestFixture]
    public class OperationsTest
    {
        [Test]
        [TestCase("6", "2", "3")]
        [TestCase("56088", "123", "456")]
        [TestCase("1", "1", "1")]
        [TestCase("64", "8", "8")]
        public void MultiplicationTest(string exp, string a, string b)
        {
            string act = Operations.Multiply(a, b);
            Assert.AreEqual(exp, act);
        }

        [Test]
        [TestCase("5", "2", "3")]
        [TestCase("56088", "55080", "1008")]
        [TestCase("61088", "55080", "6008")]
        [TestCase("1", "1", "0")]
        public void AdditionTest(string exp, string a, string b)
        {
            string act = Operations.Addition(a, b);
            Assert.AreEqual(exp, act);
        }


        [Test]
        [TestCase("9", "2", "3")]
        public void AdditionMusltiplicationTest(string exp, string a, string b)
        {
            string act = Operations.Addition(Operations.Multiply(a, b), b);
            Assert.AreEqual(exp, act);
        }
    }
}
