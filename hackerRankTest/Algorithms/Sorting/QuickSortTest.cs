﻿using NUnit.Framework;
using System;
using Utilities.Algorithms.Sorting;

namespace hackerRankTest.Algorithms.Sorting
{
    [TestFixture]
    class QuickSortTest
    {
        [Test]
        [TestCase("1,2,4,5,7", new Int32[] { 4, 1, 7, 2, 5 })]
        [TestCase("1,2,2,4,5,7,8,9", new Int32[] { 4, 8, 1, 7, 2, 5, 9, 2 })]
        [TestCase("1,2,2,3,4,5,7,8,9,12,22,34,45,56,87", new Int32[] { 4, 8, 1, 7, 2, 5, 9, 2, 22, 45, 34, 87, 3, 56, 12 })]
        public void QSortTest(string exp, Int32[] a)
        {
            IComparable[] act = SortHelpers.ToComparable(a);
            QuickSort.Sort(act);
            int[] actB = SortHelpers.ToInt(act);
            Assert.AreEqual(exp, string.Join(",", actB));
        }

        [Test]
        [TestCase("1,2,4,5,7", new Int32[] { 4, 1, 7, 2, 5 })]
        [TestCase("1,2,2,4,5,7,8,9", new Int32[] { 4, 8, 1, 7, 2, 5, 9, 2 })]
        [TestCase("1,2,2,3,4,5,7,8,9,12,22,34,45,56,87", new Int32[] { 4, 8, 1, 7, 2, 5, 9, 2, 22, 45, 34, 87, 3, 56, 12 })]
        public void QSort3WayTest(string exp, Int32[] a)
        {
            IComparable[] act = SortHelpers.ToComparable(a);
            QuickSort.Sort3Way(act);
            int[] actB = SortHelpers.ToInt(act);
            Assert.AreEqual(exp, string.Join(",", actB));
        }
    }
}
