﻿using System;
using Utilities.Algorithms.DynamicProgr;
using NUnit.Framework;

namespace hackerRankTest.Algorithms.DynamicProgr
{
    [TestFixture]
    public class MaxSubarrayTest
    {
        [Test]
        [TestCase(10, new int[] { 2, -1, 2, 3, 4, -5})]
        [TestCase(11, new int[] { 1, 2, 3, 5 })]
        [TestCase(5, new int[] { 2, -3, 2, -1, 4, -5 })]
        public void CheckmaxSubArray(int exp, int[] arr)
        {
            int leftPos = 0,
                rightPos = 0;
            int act = MaxSubarray.CountContigousSubArraySum(arr, out leftPos, out rightPos);
            Assert.AreEqual(exp, act);
        }
    }
}