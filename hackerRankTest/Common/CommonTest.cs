﻿using NUnit.Framework;
using Utilities.Common;

namespace hackerRankTest.Common
{
    [TestFixture]
    public class CommonTest
    {
        [Test]
        [TestCase(new int[] { 2, 3, 1 }, "231")]

        public void StringIntSplit(int[] exp, string num)
        {
            int[] act = num.SplitToIntArray();
            Assert.AreEqual(exp.Length, act.Length);
            Assert.AreEqual(string.Join("", exp), string.Join("", act));
        }
    }
}
