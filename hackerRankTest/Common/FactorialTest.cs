﻿using NUnit.Framework;
using Utilities.Common;

namespace hackerRankTest.Common
{
    [TestFixture]
    public class FactorialTest
    {
        [Test]
        [TestCase(1,1)]
        [TestCase(2, 2)]
        [TestCase(6, 3)]
        [TestCase(1, 0)]
        [TestCase(479001600, 12)]
        public void FactorialShouldPass(int exp, int num)
        {
            int act = Factorial.CountFactorial(num);

            Assert.AreEqual(exp, act);
        }
    }
}
